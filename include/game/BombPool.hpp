/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** BombPool
*/

#ifndef BOMBPOOL_HPP_
#define BOMBPOOL_HPP_

#include "Bomb.hpp"
#include "general.hpp"
#include "Map.hpp"
#include "SoundFX.hpp"

namespace Game
{
    class BombPool
    {
    public:
        BombPool();
        ~BombPool();
        void initPLayerBomb(int nb);
        bool dropBomb(Vector3 position, int _id_player);
        void addBombPlayer(int _id_player);
        void setBombPlayer(int _id_player, int nb);
        void addRangePlayer(int _id_player);
        void SetRangePlayer(int _id_player, int range);
        void drawBomb(void);
        int getBombPlayer(int _id_player);
        void checkBomb(void);
        void moveExplosion(Game::Map *map, std::vector<Vector3> &_p_chara, std::vector<Character*> &chara);
    protected:
        std::vector<Game::Bomb> _bombs;
    private:
        std::map<int, std::pair<int, int>> _bomb_range_player;
        Sound _fxbomb = LoadSound("../ressources/sound/laser1.ogg");
        
    };
};

#endif /* !BOMBPOOL_HPP_ */
