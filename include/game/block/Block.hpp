/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Block
*/

#ifndef BLOCK_HPP_
#define BLOCK_HPP_

#include "general.hpp"
#include "ModelCube.hpp"

namespace Game {
    enum TYPE_BLOCK {
        EMPTY_BLOCK,
        BREAKABLE_BLOCK,
        UNBREAKABLE_BLOCK,
        PLAYER_BLOCK,
        WALL_BLOCK
    };

    class Block :public Engine::ModelCube {
        public:
            Block(float x, float y, float z, float size, TYPE_BLOCK t);
            ~Block();
            void setType(TYPE_BLOCK type);
            TYPE_BLOCK getType(void) const;
        protected:
            TYPE_BLOCK _type;
        private:
    };
}

#endif /* !BLOCK_HPP_ */
