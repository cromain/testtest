/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Position
*/

#ifndef POSITION_HPP_
#define POSITION_HPP_

#include "../general.hpp"

class Velocity {
    public:
        Velocity() : _v(0.1f) {}
        ~Velocity() {}
        float getVelocity(void) const {return _v;}
        void setVelocity(float v) {_v = v;};

    protected:
        float _v;
    private:
};

#endif /* !POSITION_HPP_ */
