/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Vie
*/

#ifndef VIE_HPP_
#define VIE_HPP_

#include "../general.hpp"

class Life {
    public:
        Life();
        Life(int life);
        ~Life();
        int getLife() const;
        void setLife(int a);
        void addLife(void);
        void decrementLife(void);

    protected:
        int _life;
    private:
};

#endif /* !VIE_HPP_ */
