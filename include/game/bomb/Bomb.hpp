/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Bombe
*/

#ifndef BOMBE_HPP_
#define BOMBE_HPP_

#include "Box.hpp"
#include "Chrono.hpp"
#include "BoxPool.hpp"
#include "Sphere.hpp"
#include "SoundFX.hpp"

namespace Game {
    class Bomb : public BoxPool, Engine::Sphere, Chrono {
        public:
            Bomb(float x, float y, float z, int id_player, int range = 1);
            ~Bomb();

            int getIdPlayer() const;
            bool checkTime();
            void drawBombe(Sound bomb) const;
            int getRange(void) const;
            using Chrono::getTimerMillisecond;
            using Sphere::getPosition;
        protected:
            int _id_player;
            bool _set;
            int _rangeSize;
        private:
    };
};

#endif /* !BOMBE_HPP_ */
