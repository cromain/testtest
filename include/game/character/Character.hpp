/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Character
*/

#ifndef CHARACTER_HPP_
#define CHARACTER_HPP_

#include "ICharacter.hpp"
#include <cmath>

namespace Game
{
    class Character : public Engine::Animation, Velocity, Life, ICharacter
    {
    public:
        Character(int id, Vector3 &p, Vector3 &z, int *load, bool bot);
        ~Character();
        void draw(int bomb);
        void draw1(int bomb);
        void draw2(int bomb);
        virtual void move(Game::Map *map);
        virtual void move2(Game::Map *map);
        virtual void move3(Game::Map *map);
        virtual void move4(Game::Map *map);
        void setPosition(float x, float y, float z);
        bool willColide(std::vector<std::vector<Game::Block>> map, Vector3 pos);
        virtual bool checkIfDropBombe();
        bool character;

        using Velocity::getVelocity;
        using Velocity::setVelocity;
        using Life::decrementLife;
        using Life::addLife;
        using Life::getLife;
    protected:
        int _id;
        Texture2D _texture;
        Model _model;
        Vector3 &_position;
        Vector3 &_posbomb;
        Color _color;
        float _yaw;
        float _scale;
        int _moveup;
        int _movedown;
        int _moveleft;
        int _moverigth;
    private:
    };
};
#endif /* !CHARACTER_HPP_ */
