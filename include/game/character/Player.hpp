/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Player
*/

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "Character.hpp"

namespace Game {
    class Player : public Character {
        public:
            Player();
            ~Player();

        protected:
        private:
    };
};

#endif /* !PLAYER_HPP_ */