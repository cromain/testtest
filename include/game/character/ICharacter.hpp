/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** ICharacter
*/

#ifndef ICHARACTER_HPP_
#define ICHARACTER_HPP_

#include "general.hpp"
#include "Velocity.hpp"
#include "Life.hpp"
#include "Map.hpp"
#include "ModelCube.hpp"
#include "Animation.hpp"


namespace Game {
    class ICharacter {
        public:

        virtual void draw(int bomb) = 0;
        virtual void move(Game::Map *map) = 0;
        virtual void setPosition(float x, float y, float z) = 0;
        virtual bool willColide(std::vector<std::vector<Game::Block>> map, Vector3 pos) = 0;
        virtual bool checkIfDropBombe() = 0;
    };
}

#endif /* !ICHARACTER_HPP_ */