/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** BoxPool
*/

#ifndef BOXPOOL_HPP_
#define BOXPOOL_HPP_

#include "general.hpp"
#include "Box.hpp"
#include "Map.hpp"
#include "Character.hpp"
#include "SoundFX.hpp"

class BoxPool {
    public:
        BoxPool(float x, float y, float z, int range = 1);
        ~BoxPool();

        void moveExplosion(Game::Map *map, std::vector<Vector3> &_p_chara, std::vector<Game::Character*> &chara);
        void drawExplosion() const;
        void addRangeVectorExplosion(Vector3 &a, float z, float x, int index);
        std::vector<float> getRangeAllDirection() const;
    protected:
        std::vector<std::vector<Vector3>> _poolVector;
        Engine::Box _fireBox;
        Vector3 _p_start;
        std::vector<Vector3> _position_explosion;
        std::vector<float> _range;
    private:
};

#endif /* !BOXPOOL_HPP_ */
