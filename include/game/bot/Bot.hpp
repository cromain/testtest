/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Bot
*/

#ifndef BOT_HPP_
#define BOT_HPP_

#include "Character.hpp"
#include "BombPool.hpp"
#include  <tgmath.h> 
namespace Game {
    class Bot : public Character {
        public:
            Bot(int id, Vector3 &p, std::vector<Vector3> &p_chara, std::vector<Game::Bomb> &bombs, int *load) : Character(id, p, p, load, false), _p_chara(p_chara), _bombs(bombs), _dropBomb(false) {};
            ~Bot();
            void move(Game::Map *map);
            void moveAnotherDirection(Game::Map *map, bool up);
            bool checkIfDropBombe();
            bool isInBombRange(void);
            Vector3 chooseTarget(void);
            bool isInBombRange(float z, float x);
            bool willColideWithBreakable(std::vector<std::vector<Game::Block>> map, Vector3 pos);
            void getPathEscape(Game::Map *map);
            std::vector<Vector3> checkWay(std::vector<Vector3> pos, Game::Map *map);
            void goToTarget(Game::Map *map);
            bool willColide(std::vector<std::vector<Game::Block>> map, Vector3 pos);

        protected:
            std::vector<Vector3> &_p_chara;
            std::vector<Game::Bomb> &_bombs;
            Vector3 _target;
            std::vector<Vector3> _target_escape;
            bool _dropBomb;
        private:
    };
}
#endif /* !BOT_HPP_ */
