/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** BackupManager
*/

#ifndef BACKUPMANAGER_HPP_
#define BACKUPMANAGER_HPP_

#include "Gameplay.hpp"
#include "general.hpp"
#include <iostream>
#include <fstream>
#include "Block.hpp"

class BackupManager {
    public:
        BackupManager();
        ~BackupManager();

        std::vector<std::string> getBackups(void);
        bool saveGame(Screen::Gameplay toSave, std::string filename);
        Screen::Gameplay loadGame(void);
    protected:
    private:
};

#endif /* !BACKUPMANAGER_HPP_ */
