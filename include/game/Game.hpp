/*
** EPITECH PROJECT, 2021
** Game.hpp
** File description:
** Game
*/

#ifndef GAME_HPP_
#define GAME_HPP_

#include <vector>
#include "Menu.hpp"
#include "Lobby.hpp"
#include "Gameplay.hpp"
#include "Settings.hpp"
#include "GameUI.hpp"
#include "Engine.hpp"
#include "SoundMenu.hpp"
#include "SoundFX.hpp"

typedef enum {
    MENU = 0,
    LOBBY,
    GAMEPLAY,
    SETTINGS,
    SOUNDMENU
} GameScreen;

namespace Game {
    class Game : public Engine::Camera, public Mouse {
        public:
            Game();
            ~Game();

            void handleEvent();
            void displayScreen();

        protected:
        private:
            GameScreen _currentScreen;
            Engine::Engine *_engine;
            std::vector<IScreen *> _screens;
            IScreen *_soundMenu;
            GameUI *_ui;
            GameUI *_ui2;
            GameUI *_ui3;
            GameUI *_ui4;
            bool _sound_m;
    };
};

#endif /* !GAME_HPP_ */