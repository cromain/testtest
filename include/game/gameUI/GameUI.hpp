/*
** EPITECH PROJECT, 2021
** GameUI.hpp
** File description:
** GameUI
*/

#ifndef GAMEUI_HPP_
#define GAMEUI_HPP_

#include "general.hpp"

namespace Game {
    class GameUI {
        public:
            GameUI(std::string name);
            ~GameUI();
            void posUI(float x, float y);
            bool isPlayer(bool isplayer);
            void setLife(int life);
            void setBomb(int bomb);
            bool getIsPlayer();
            void drawUI();
            std::string getName() {return _name;};

        protected:
            
        private:
            std::string _name;
            bool _isplayer;
            float _posx;
            float _posy;
            Texture2D _heart;
            Texture2D _bomb;
            Texture2D _headskull;
            int _life;
            int _nbbomb;
    };
}

#endif /* !GAMEUI_HPP_ */