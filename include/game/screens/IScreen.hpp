/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** IScreen
*/

#ifndef ISCREEN_HPP_
#define ISCREEN_HPP_

#include "Button.hpp"
#include "Map.hpp"
#include "Mouse.hpp"
#include "Camera.hpp"
#include "BombPool.hpp"
#include "GameUI.hpp"
#include "Character.hpp"

class IScreen : public Mouse{
    public:

        virtual void drawScreen(void) = 0;
        virtual void handleKeyboard(int *_currentScreen, bool *_sound_m) = 0;
        virtual void handleMouse(int *_currentScreen) = 0;

    protected:
    private:
};

#endif /* !ISCREEN_HPP_ */
