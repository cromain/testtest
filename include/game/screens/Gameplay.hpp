/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Gameplay
*/

#ifndef GAMEPLAY_HPP_
#define GAMEPLAY_HPP_

#include "IScreen.hpp"
#include "Bot.hpp"
#include "Block.hpp"
#include "SoundFX.hpp"

namespace Screen {
    class Gameplay : public IScreen, Engine::Camera, Game::BombPool {
        public:
            Gameplay();
            ~Gameplay();

            void drawScreen(void);
            void handleMouse(int *_currentScreen);
            void handleKeyboard(int *_currentScreen, bool *_sound_m);
            void update(void);
            void drawCharacters(void);
            void initGame(std::vector<Game::GameUI> loadstat);
            void checkEnd(void);

            std::vector<std::vector<Game::Block>> getVectorMap();
            std::vector<Vector3> getCharacterPos(void);
        protected:
        private:
            Game::Map *_map;
            std::vector<Game::Character *> _characters;
            std::vector<Vector3> _p_chara;
            std::vector<Vector3> _p_bomb;
            Texture2D _mainBackgroundPlay;
            Texture2D _background;
            bool _pause;
            int _nb_player;
            std::vector<Game::GameUI> _stat;
            int frames;
            Texture2D _recScore;
            Texture2D _rectScore;
            Game::SoundFX _bomb;
            bool _isEnd;
            bool _isDraw;
            Rectangle _r_lobby;
            Rectangle _r_sound;
    };
};

#endif /* !GAMEPLAY_HPP_ */
