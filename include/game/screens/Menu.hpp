/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Menu
*/

#ifndef MENU_HPP_
#define MENU_HPP_

#include "IScreen.hpp"

namespace Screen {
    class Menu : public IScreen {
        public:
            Menu();
            ~Menu();

            void update(void);
            void drawScreen(void);
            void handleKeyboard(int *_currentScreen, bool *_sound_m);
            void handleMouse(int *_currentScreen);

        protected:
        private:
            Texture _texture;
            Font _font;
            std::vector<Engine::Button *> _buttons;
            int _frames;
    };
};
#endif /* !MENU_HPP_ */
