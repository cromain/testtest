/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Menu
*/

#ifndef LOBBY_HPP_
#define LOBBY_HPP_

#include "raylib.h"
#include <vector>
#include <string>
#include "IScreen.hpp"
#include <Gameplay.hpp>

namespace Screen {
    class Lobby : public IScreen
    {
        public:
            Lobby();
            ~Lobby();

            void drawScreen(void);
            void handleMouse(int *_currentScreen);
            void handleKeyboard(int *_currentScreen, bool *_sound_m);
            std::vector<Game::GameUI> prealoadGame();

            enum _SKINS_
            {
                VILLAGER,
                ZOMBIE,
                TEST,
                CHAR
            };

        protected:
        private:
            int _nbPlayers = 1;
            std::vector<int> _players = {-1, -1, -1, -1};
            Texture2D _background;
            Texture2D _man;
            Texture2D _girl;
            Texture2D _logo;
            std::vector<std::pair<int, _SKINS_>> _playerSkins;
            Font _font;
            std::vector<Game::GameUI> _stat;
    };
};

#endif /* !LOBBY_HPP_ */
