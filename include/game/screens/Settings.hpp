/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Settings
*/

#ifndef SETTINGS_HPP_
#define SETTINGS_HPP_

#include "IScreen.hpp"

namespace Screen {
    class Settings : public IScreen {
        public:
            Settings();
            ~Settings();

            void drawScreen(void);
            void handleMouse(int *_currentScreen);
            void handleKeyboard(int *_currentScreen, bool *_sound_m);

        protected:
        private:
            Font _font;
    };
};

#endif /* !SETTINGS_HPP_ */
