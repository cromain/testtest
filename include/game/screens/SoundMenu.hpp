/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** SoundMenu
*/

#ifndef SOUNDMENU_HPP_
#define SOUNDMENU_HPP_

#include "IScreen.hpp"

namespace Screen {
    class SoundMenu : public IScreen {
        public:
            SoundMenu();
            ~SoundMenu();

            void drawScreen(void);
            void handleKeyboard(int *_currentScreen, bool *_sound_m);
            void handleMouse(int *_currentScreen);

        protected:
            Sound _music;
            float _volume;
            Rectangle _plus;
            Rectangle _moins;
        private:
    };
};

#endif /* !SOUNDMENU_HPP_ */