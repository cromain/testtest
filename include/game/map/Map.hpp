/*
** EPITECH PROJECT, 2021
** Map.hpp
** File description:
** Map
*/

#ifndef MAP_HPP_
#define MAP_HPP_

#include "PowerUp.hpp"
#include "Block.hpp"

namespace Game {
    class Map : public PowerUp {
        public:
            Map(int width, int height);
            ~Map();

            void init(void);
            void generation(void);
            void Display(void);
            void fillMap(void);
            void freeSpaceForPlayer(void);
            void setModelBoxes(void);
            int getWidth(void) const;
            int getHeight(void) const;
            void breakWall(int z, int x);
            Game::TYPE_BLOCK getTypeBlock(int z, int x) const;
            std::vector<std::vector<Block>> &getMap(void);

        protected:
            int _width;
            int _height;
            std::vector<Game::Block> _model_texture;
            std::vector<std::vector<Block>> _map;
            Block _sol;
        private:
    };
}

#endif /* !MAP_HPP_ */