/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** PowerUp
*/

#ifndef POWERUP_HPP_
#define POWERUP_HPP_

#include "Square.hpp"

namespace Game {
    class PowerUp {
        public:
            PowerUp();
            ~PowerUp();
            void RandomAddPowerUp(float x, float y, float z);
            std::vector<Engine::Square> &getPowerUp();
            void AddPowerUp(float x, float y, float z, int type);
            void drawPowerUp() const;
            int isOnBonus(float z, float x);

    
        protected:
            std::vector<Engine::TextureClass> _powerUpTexture;
            std::vector<Engine::Square> _powerUps;
            std::vector<int> _t;
        private:
    };
}

#endif /* !POWERUP_HPP_ */
