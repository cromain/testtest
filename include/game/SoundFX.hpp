/*
** EPITECH PROJECT, 2021
** Sound.hpp
** File description:
** Sound
*/

#ifndef SOUND_HPP_
#define SOUND_HPP_

#include "general.hpp"

namespace Game {
    class SoundFX {
        public:
            SoundFX();
            ~SoundFX();
            void fx();

        protected:
        private:
            Sound _fxbomb;
    };
}

#endif /* !SOUND_HPP_ */