/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Mouse
*/

#ifndef MOUSE_HPP_
#define MOUSE_HPP_

#include "raylib.h"

class Mouse {
    public:
        Mouse() = default;
        ~Mouse() = default;

        bool isClicked(int mouseButton) {return IsMouseButtonDown(mouseButton);}
        bool isOnRect(Rectangle rect) {return CheckCollisionPointRec(GetMousePosition(), rect);}
        bool isRealeased(int mouseButton) {return IsMouseButtonReleased(mouseButton);}
        Vector2 getPositionMouse(void) {return GetMousePosition();};

    protected:
    private:
};
#endif /* !MOUSE_HPP_ */