/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Chrono
*/

#ifndef CHRONO_HPP_
#define CHRONO_HPP_

#include "../general.hpp"

class Chrono {
    public:
        Chrono();
        ~Chrono() {}
        void setChrono();

        std::chrono::steady_clock::time_point getChrono();
        std::chrono::steady_clock::time_point now();
        std::chrono::duration<double> getTimerMillisecond(std::chrono::steady_clock::time_point) const;
    private:
        std::chrono::steady_clock::time_point _now;
};


#endif /* !CHRONO_HPP_ */
