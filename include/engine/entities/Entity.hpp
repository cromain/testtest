/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Entity
*/

#ifndef ENTITY_HPP_
#define ENTITY_HPP_

#include "TextureClass.hpp"
#include "general.hpp"

namespace Engine {
    class IObject {
        public:
            virtual void draw(void) const = 0;
    };

    class Entity :public TextureClass, IObject {
        public:
            Entity(float x, float y, float z, float size);
            Entity(float x, float y, float z);
            // position
            void setPosition(float x, float y, float z);
            Vector3 getPosition(void) const;
            void setPositionVector(Vector3 a);
            // MODEL
            void loadModel(const char *a);
            void setModel(Model &model);
            Model &getModel(void);
            void unloadModel(void);
            // Type Of Block
            // SIZE
            void setSize(float height);
            float getSize(void) const;
            // HEIGHT
            void setHeight(float height);
            float getHeight(void) const;

        protected:
            Model _model;
            Vector3 _position;
            float _size;
            float _height;
        private:
    };
}

#endif /* !ENTITY_HPP_ */
