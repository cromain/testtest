/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** TextureClass
*/

#ifndef TEXTURECLASS_HPP_
#define TEXTURECLASS_HPP_

#include "raylib.h"

namespace Engine {
    class TextureClass {
        public:
            TextureClass(const char *str);
            TextureClass();
            ~TextureClass();
            void setTexture(Texture2D &_a);
            Texture2D &getTexture(void);
            Texture2D getTexture(void) const;
            void loadTexture(const char *str);
            void unloadTexture();

        protected:
            Texture2D _texture;
        private:
    };
}
#endif /* !TEXTURECLASS_HPP_ */
