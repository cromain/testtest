/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Button
*/

#ifndef BUTTON_HPP_
#define BUTTON_HPP_

#include "raylib.h"
#include "Entity.hpp"

#define S_MENU 0
#define S_LOBBY 1
#define S_GAMEPLAY 2
#define S_SETTINGS 3

namespace Engine {

    typedef enum
    {
        NONE = -1,
        PLAY,
        SETTINGS,
        QUIT
    } ButtonType;

    class Button : public Entity {
        public:
            Button(float x, float y, const char *base, const char *mouse_on, const char *clicked, ButtonType type);
            ~Button();
    
            void setPosition(float x, float y);
            void draw(void) const;
            void setStatus(Vector2 mouse, int *_currentScreen);
            Rectangle getRect(void) {return _rect;}

        protected:
        private:
            Vector2 _position;
            std::vector<Texture2D> _textures;
            Rectangle _rect;
            int _buttonStatus;
            bool _buttonAction;
            Sound _sound;
            ButtonType _type;
    };
};

#endif /* !BUTTON_HPP_ */