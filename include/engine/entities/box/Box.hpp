/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Box
*/

#ifndef BOX_HPP_
#define BOX_HPP_

#include "Entity.hpp"

namespace Engine {
    class Box : public Entity {
        public:
            Box(float x, float y, float z, float size);
            ~Box();
            void draw(void) const;
            void drawAtPosition(Vector3 a) const;
            void setWireVisible(bool visible);
            void setColor(Color color);
            void setIs_texture(bool a);
        protected:
            bool _wireVisible = false;
            Color _color;
            bool _is_texture;
        private:
    };
}

#endif /* !BOX_HPP_ */
