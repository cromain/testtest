/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Sphere
*/

#ifndef SPHERE_HPP_
#define SPHERE_HPP_

#include "Entity.hpp"
#include "raylib.h"

namespace Engine {
    class Sphere : public Entity {
        public:
            Sphere(float x, float y, float z, float radius) : Entity(x, y, z) {_radius = radius;};
            ~Sphere();
            void draw(void) const;
            void setWireVisible(bool visible);
            void setWireParameters(int rings, int slices, Color color);
            void setColor(Color color);
        protected:
            bool _wireVisible = false;
            Color _wireColor = BLACK;
            Color _color = WHITE;
            float _radius;
            int _rings = 10;
            int _slices = 10;
        private:
    };
}

#endif /* !SPHERE_HPP_ */
