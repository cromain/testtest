/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Square
*/

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "Box.hpp"

namespace Engine {
    class Square : public Box {
        public:
            Square(float x, float y, float z, float size, Texture2D &_texture);
            ~Square();

        protected:
        private:
    };
};

#endif /* !SQUARE_HPP_ */
