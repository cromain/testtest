/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Model
*/

#ifndef MODEL_HPP_
#define MODEL_HPP_

#include "Entity.hpp"

namespace Engine {
    class ModelCube :public Entity {
        public:
            ModelCube(float x, float y, float z, float size);
            ~ModelCube();
            void draw(void) const;
        protected:
            bool _wireVisible = false;
        private:
    };
}

#endif /* !MODEL_HPP_ */
