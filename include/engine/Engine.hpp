/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Engine
*/

#ifndef ENGINE_HPP_
#define ENGINE_HPP_

#include <string>
#include "raylib.h"
#include <time.h>
#include <iostream>

namespace Engine {
    class Engine {
        public:
            Engine();
            ~Engine();

            void start(int screenWidth, int screenHeight,std::string windowName);
            void updateFPS(int fps);
            void toggleFullScreen(void);
            void beginDraw(void);
            void endDraw(void);
            int getScreenWidth(void) {return _screenWidth;}
            int getScreenHeight(void) {return _screenHeight;}

        protected:
        private:
            int _screenWidth = 800;
            int _fpsLimiter = 60;
            int _screenHeight = 450;
            std::string _windowName = "";
            bool _isFullScreen = false;
    };
}
#endif /* !ENGINE_HPP_ */
