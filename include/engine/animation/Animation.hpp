/*
** EPITECH PROJECT, 2021
** Animation.hpp
** File description:
** Animation
*/

#ifndef ANIMATION_HPP_
#define ANIMATION_HPP_

#include "general.hpp"

namespace Engine {
    class Animation {
        public:
            Animation(int id, int *load);
            ~Animation();
            void my_loadmodelDrop(std::string path, int nb, int *load);
            void my_loadmodel(std::string path, int nb, int *load);
            void my_loader(int load, int check);
        protected:
            int frame;
            int speed;
            Model model;
            std::string obj;
            std::vector<Model> vecModel;
            std::vector<Model> vecModelDrop;
            int speedDrop;
            bool drop;
    };
}


#endif /* !ANIMATION_HPP_ */