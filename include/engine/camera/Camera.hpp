/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Camera
*/

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include "raylib.h"

namespace Engine {
    class Camera;
}

class Engine::Camera {
    public:
        Camera();
        ~Camera();
        void setPosition(float x, float y, float z);
        Vector3 getPosition(void) const;
        void setTarget(float x, float y, float z);
        Vector3 getTarget(void) const;
        void setUp(float x, float y, float z);
        Vector3 getUp(void) const;
        void setFovY(float fovY);
        float getFovY(void);
        void setMode(CameraMode mode);
        void setProjection(CameraProjection projection);
        void update(void);
        void start3DMode(void);
        void end3DMode(void);
        Camera3D getCamera(void);
    protected:
    private:
        Camera3D _camera;
};

#endif /* !CAMERA_HPP_ */
