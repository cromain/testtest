/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** General
*/

#ifndef GENERAL_HPP_
#define GENERAL_HPP_

#define BACKUP_REPOSITORY "../save/"

#include <chrono>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <iterator>
#include <map>
#include <cstdlib>
#include "raylib.h"
#include "rlgl.h"
#include <sys/types.h>
#include <dirent.h>

#endif /* !GENERAL_HPP_ */
