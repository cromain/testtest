/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Engine
*/

#include "Engine.hpp"

void LogCustom(int msgType, const char *text, va_list args)
{
    char timeStr[64] = { 0 };
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);

    strftime(timeStr, sizeof(timeStr), "%Y-%m-%d %H:%M:%S", tm_info);
    printf("[%s] ", timeStr);

    switch (msgType)
    {
        case LOG_INFO: printf("[INFO] : "); break;
        case LOG_ERROR: printf("[ERROR]: "); break;
        case LOG_WARNING: printf("[WARN] : "); break;
        case LOG_DEBUG: printf("[DEBUG]: "); break;
        default: break;
    }

    vprintf(text, args);
    printf("\n");
}

Engine::Engine::Engine()
{
    start(1600, 800, "Indie Studio");
    updateFPS(60);
}

Engine::Engine::~Engine()
{
}

void Engine::Engine::start(int screenWidth, int screenHeight,std::string windowName)
{
    Image icon = LoadImage("../ressources/bomberman.png");
    ImageFormat(&icon, PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
    std::cout  << icon.format << "=" << PIXELFORMAT_UNCOMPRESSED_R8G8B8A8 << std::endl;
    InitWindow(screenWidth, screenHeight, windowName.c_str());
    SetWindowState(FLAG_WINDOW_RESIZABLE);
    _windowName = windowName;
    _screenWidth = screenWidth;
    _screenHeight = _screenHeight;
    SetTraceLogCallback(LogCustom);
    SetWindowIcon(icon);
    // toggleFullScreen();
}

void Engine::Engine::updateFPS(int fps)
{
    _fpsLimiter = fps;
    SetTargetFPS(_fpsLimiter);
}

void Engine::Engine::toggleFullScreen(void)
{
    ToggleFullscreen();
    _isFullScreen = !_isFullScreen;
}

void Engine::Engine::beginDraw(void)
{
    BeginDrawing();
    ClearBackground(RAYWHITE);
}

void Engine::Engine::endDraw(void)
{
    EndDrawing();
}