/*
** EPITECH PROJECT, 2021
** Animation.cpp
** File description:
** Animation
*/

#include "Animation.hpp"
#include "general.hpp"

Engine::Animation::Animation(int id, int *load) : drop(false)
{
    switch (id)
    {
    case 1:
        my_loadmodel("../ressources/runeman/persorunmanobj", 34, load);
        my_loadmodelDrop("../ressources/mancroutch/croutchman", 38, load);
        my_loader(*load, 1);
        break;
    case 2:
        my_loadmodel("../ressources/rungirl/persorungirlobj", 34, load);
        my_loadmodelDrop("../ressources/girlcroutch/croutchgirl", 38, load);
        my_loader(*load, 1);
        break;
    case 3:
        my_loadmodel("../ressources/rungirl/persorungirlobj", 34, load);
        my_loadmodelDrop("../ressources/girlcroutch/croutchgirl", 38, load);
        my_loader(*load, 1);
        break;
    case 4:
        my_loadmodel("../ressources/runeman/persorunmanobj", 34, load);
        my_loadmodelDrop("../ressources/mancroutch/croutchman", 38, load);
        my_loader(*load, 1);
        break;
    default:
        break;
    }
}

Engine::Animation::~Animation()
{
    for (int i = 0; i <= 33; i++)
        UnloadModel(vecModel[i]);
    for (int i = 0; i <= 38; i++)
        UnloadModel(vecModelDrop[i]);
}


void Engine::Animation::my_loadmodel(std::string path, int nb, int *load)
{
    for (int i = 1; i <= nb; i++) {
        my_loader(*load,0);
        if (i >= 10 && i < 100)
            obj = path + "_0000" + std::to_string(i) + ".obj";
        else if (i >= 100)
            obj = path + "_000" + std::to_string(i) + ".obj";
        else
            obj = path + "_00000" + std::to_string(i) + ".obj";
        model = LoadModel(obj.c_str());
        vecModel.push_back(model);
        obj.clear();
        if (*load < 1000)
            (*load)+=2;
    }

    for(int i = 1; i < nb; i++) {
        my_loader(*load,0);
        vecModel[i].materials = vecModel[0].materials;
        if (*load < 1000)
            (*load)+=2;
    }
}

void Engine::Animation::my_loadmodelDrop(std::string path, int nb, int *load)
{
    for (int i = 1; i <= nb; i++) {
        my_loader(*load, 0);
        if (i >= 10 && i < 100)
            obj = path + "_0000" + std::to_string(i) + ".obj";
        else if (i >= 100)
            obj = path + "_000" + std::to_string(i) + ".obj";
        else
            obj = path + "_00000" + std::to_string(i) + ".obj";
        model = LoadModel(obj.c_str());
        vecModelDrop.push_back(model);
        obj.clear();
        if (*load < 1000)
            (*load)+=2;
    }

    for(int i = 1; i < nb; i++) {
        my_loader(*load, 0);
        vecModelDrop[i].materials = vecModelDrop[0].materials;
        if (*load < 1000)
            (*load)+=2;
    }
}

void Engine::Animation::my_loader(int load, int check)
{
    BeginDrawing();
    ClearBackground(RAYWHITE);
    if (load % 50 <= 25)
        DrawText("LOADING DATA...", 640, 110, 40, DARKBLUE);
    if (check == 1)
        DrawText("LOADING DATA...", 640, 110, 40, DARKBLUE);
    DrawRectangle(300, 200, load, 60, SKYBLUE);
    DrawRectangleLines(300, 200, 1000, 60, DARKGRAY);
    EndDrawing();    
}