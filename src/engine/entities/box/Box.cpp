/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Box
*/

#include "Box.hpp"

Engine::Box::Box(float x, float y, float z, float size)
:Entity(x, y, z, size), _is_texture(true)
{

}

Engine::Box::~Box()
{
}

void Engine::Box::draw(void) const
{
    if (!_is_texture) {
        DrawCube(getPosition(), _size, _height, _size, _color);
    }
    else {
        DrawCubeTexture(getTexture(), getPosition(), _size, _height, _size, WHITE);
        if (_wireVisible)
            DrawCubeWires(getPosition(), _size, _height, _size, WHITE);
    }
}

void Engine::Box::drawAtPosition(Vector3 a) const
{
    if (!_is_texture) {
        DrawCube(a, _size, _height, _size, _color);
    }
    else {
        DrawCubeTexture(getTexture(), a, _size, _height, _size, WHITE);
        if (_wireVisible)
            DrawCubeWires(a, _size, _height, _size, WHITE);
    }
}

void Engine::Box::setWireVisible(bool visible) {
    _wireVisible = visible;
}

void Engine::Box::setColor(Color color)
{
    _color = color;
}

void Engine::Box::setIs_texture(bool a)
{
    _is_texture = a;
}
