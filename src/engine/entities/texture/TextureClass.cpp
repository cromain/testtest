/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** TextureClass
*/

#include "TextureClass.hpp"

Engine::TextureClass::TextureClass()
{
}

Engine::TextureClass::TextureClass(const char *str)
{
    loadTexture(str);
}

Engine::TextureClass::~TextureClass()
{
}

void Engine::TextureClass::setTexture(Texture2D &a)
{
    _texture = a;
}

Texture2D &Engine::TextureClass::getTexture(void)
{
    return (_texture);
}

void Engine::TextureClass::loadTexture(const char *str)
{
    _texture = LoadTexture(str);
}

Texture2D Engine::TextureClass::getTexture(void) const
{
    return _texture;
}


void Engine::TextureClass::unloadTexture()
{
    UnloadTexture(_texture);
}