/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Button
*/

#include "Button.hpp"

Engine::Button::Button(float x, float y, const char *base, const char *mouse_on, const char *clicked, ButtonType type)
: Entity(0, 0, 0, 0), _type(type)
{
    _position = (Vector2) {x, y};
    _textures.push_back(LoadTexture(base));
    _textures.push_back(LoadTexture(mouse_on));
    _textures.push_back(LoadTexture(clicked));
    _rect = (Rectangle) { float(x), float(y), float(_textures[0].width), float(_textures[0].height)};
    _sound = LoadSound("../ressources/bouton.wav");
}

Engine::Button::~Button()
{
    UnloadTexture(_textures[0]);
    UnloadTexture(_textures[1]);
    UnloadTexture(_textures[2]);
}

void Engine::Button::setStatus(Vector2 mouse, int *_currentScreen)
{
    _buttonStatus = 0;
    _buttonAction = false;

    if (CheckCollisionPointRec(mouse, _rect))
    {
        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT))
            _buttonStatus = 2;
        else
            _buttonStatus = 1;

        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT))
            _buttonAction = true;
    }
    else _buttonStatus = 0;

    if (_buttonAction) {
        PlaySound(_sound);
        switch (_type) {
            case PLAY:
            {
                (*_currentScreen) = S_LOBBY;
            } break;
            case SETTINGS:
            {
                (*_currentScreen) = S_SETTINGS;
            } break;
            case QUIT:
            {
                CloseWindow();
                exit(0);
            }
            default: break;
        }
    }
}

void Engine::Button::setPosition(float x, float y)
{
    _position.x = x;
    _position.y = y;
    _rect = (Rectangle) { float(x), float(y), float(_textures[0].width), float(_textures[0].height)};
}

void Engine::Button::draw(void) const
{
    DrawTexture(_textures[_buttonStatus], _position.x, _position.y, WHITE);
}