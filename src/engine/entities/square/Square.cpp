/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Square
*/

#include "Square.hpp"

Engine::Square::Square(float x, float y, float z, float size, Texture2D &_texture)
:Box(x, y, z, size)
{
    setHeight(0.01);
    setTexture(_texture);
}

Engine::Square::~Square()
{
}