/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Box
*/

#include "ModelCube.hpp"

Engine::ModelCube::ModelCube(float x, float y, float z, float size)
:Entity(x, y, z, size)
{

}

Engine::ModelCube::~ModelCube()
{
}

void Engine::ModelCube::draw(void) const
{
    DrawModel(_model, _position, _size, WHITE);
}