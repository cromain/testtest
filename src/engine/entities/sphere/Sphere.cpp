/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Sphere
*/

#include "Sphere.hpp"


Engine::Sphere::~Sphere()
{

}

void Engine::Sphere::draw(void) const
{
    DrawSphere(getPosition(), _radius, _color);
    if (_wireVisible) {
        DrawSphereWires(getPosition(), _radius, _rings, _slices, _wireColor);
    }
}

void Engine::Sphere::setWireVisible(bool visible)
{
    _wireVisible = visible;
}

void Engine::Sphere::setWireParameters(int rings, int slices, Color color)
{
    _rings = rings;
    _slices = slices;
    _wireColor = color;
}

void Engine::Sphere::setColor(Color color)
{
    _color = color;
}
