/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Entity
*/

#include "Entity.hpp"

Engine::Entity::Entity(float x, float y, float z, float size)
:_position(Vector3 {x, y, z}), _size(size), _height(size)
{

}

Engine::Entity::Entity(float x, float y, float z)
:_position(Vector3 {x, y, z})
{
}

// -------------------------------------POSITION
void Engine::Entity::setPosition(float x, float y, float z)
{
    _position = Vector3 {x, y, z};
}

Vector3 Engine::Entity::getPosition(void) const
{
    return _position;
}

void Engine::Entity::setPositionVector(Vector3 a)
{
    _position = a;
}
// -------------------------------------MODEL

void Engine::Entity::loadModel(const char *a)
{
    _model = LoadModel(a);
}

Model &Engine::Entity::getModel(void)
{
    return _model;
}

void Engine::Entity::setModel(Model &model)
{
    _model = model;
}

void Engine::Entity::unloadModel(void)
{
    UnloadModel(_model);
}

// -------------------------------------SIZE

void Engine::Entity::setSize(float size)
{
    _size = size;
}

float Engine::Entity::getSize(void) const
{
    return _size;
}

// -------------------------------------HEIGHT

void Engine::Entity::setHeight(float height)
{
    _height = height;
}

float Engine::Entity::getHeight(void) const
{
    return (_height);
}