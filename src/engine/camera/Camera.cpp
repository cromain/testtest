/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Camera
*/

#include "Camera.hpp"

Engine::Camera::Camera()
{
    _camera = {0};
    setPosition(8.5f, 21.0f,  14.0f);
    setTarget(8.5f , 0.0f, 6.5f);
    setUp(0.0f, 1.0f, 0.0f);
    setFovY(50.0f);
    setProjection(CAMERA_PERSPECTIVE);
}

Engine::Camera::~Camera()
{
}

void Engine::Camera::setPosition(float x, float y, float z)
{
    _camera.position = Vector3 {x, y, z};
}

Vector3 Engine::Camera::getPosition(void) const
{
    return _camera.position;
}

void Engine::Camera::setTarget(float x, float y, float z)
{
    _camera.target = Vector3 {x, y, z};
}

Vector3 Engine::Camera::getTarget(void) const
{
    return _camera.target;
}

void Engine::Camera::setUp(float x, float y, float z)
{
    _camera.up = Vector3 {x, y, z};
}

Vector3 Engine::Camera::getUp(void) const
{
    return _camera.up;
}

void Engine::Camera::setFovY(float fovY)
{
    _camera.fovy = fovY;
}

float Engine::Camera::getFovY(void)
{
    return _camera.fovy;
}

void Engine::Camera::setMode(CameraMode mode)
{
    SetCameraMode(_camera, mode);
}

void Engine::Camera::setProjection(CameraProjection projection)
{
    _camera.projection = projection;
}

void Engine::Camera::update(void)
{
    UpdateCamera(&_camera);
}

void Engine::Camera::start3DMode(void)
{
    BeginMode3D(_camera);
}

void Engine::Camera::end3DMode(void)
{
    EndMode3D();
}

Camera3D Engine::Camera::getCamera(void)
{
    return _camera;
}