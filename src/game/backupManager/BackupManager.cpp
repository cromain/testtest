/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** BackupManager
*/

#include "BackupManager.hpp"

BackupManager::BackupManager()
{
}

BackupManager::~BackupManager()
{
}



std::vector<std::string> BackupManager::getBackups(void)
{
    DIR *d;
    dirent *readedFile;
    std::vector<std::string> loaded;

    d = opendir(BACKUP_REPOSITORY);
    if (!d) {
        throw "Failed to open directory";
    }
    while ((readedFile = readdir(d)) != NULL)
       loaded.push_back(readedFile->d_name);
    return loaded;
}

bool BackupManager::saveGame(Screen::Gameplay toSave, std::string filename)
{
    std::ofstream file;
    file.open(BACKUP_REPOSITORY + filename + ".xml");
    std::vector<std::vector<Game::Block>> tmpMap = toSave.getVectorMap();
    std::vector<Vector3> tmpPChar = toSave.getCharacterPos();

    if (!file) {
        throw "Failed to open file";
        return false;
    }
    file << "<xml>" << std::endl;
    file << "   <map>" << std::endl;
    for (int i = 0; i != tmpMap.size(); i++) {
        for (int y = 0; y != tmpMap[i].size(); y++) {
            file << "       " << tmpMap[i][y].getType() << ";" << std::to_string(tmpMap[i][y].getPosition().x) << ";" << std::to_string(tmpMap[i][y].getPosition().z) << std::endl;
        }
    }
    file << "   </map>" << std::endl;
    file << "   <character_pos>" << std::endl;
    for (int i = 0; i != tmpMap.size(); i++) {
        file << "       " << std::to_string(i) << ";" << std::to_string(tmpPChar[i].x) << ";" << std::to_string(tmpPChar[i].z) << std::endl;
    }
    file << "   </character_pos>" << std::endl;
    file << "</xml>";
    return true;
}

Screen::Gameplay BackupManager::loadGame(void)
{

}
