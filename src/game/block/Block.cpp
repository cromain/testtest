/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Block
*/

#include "Block.hpp"

Game::Block::Block(float x, float y, float z, float size, TYPE_BLOCK t)
:ModelCube(x, y, z, size), _type(t)
{
}

Game::Block::~Block()
{
}

// -------------------------------------TYPE OF BLOCK

void Game::Block::setType(TYPE_BLOCK type)
{
    _type = type;
}

Game::TYPE_BLOCK Game::Block::getType(void) const
{
    return (_type);
}