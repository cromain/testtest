/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** BoxPool
*/

#include "BoxPool.hpp"

BoxPool::BoxPool(float x, float y, float z, int range)
:_fireBox(x, y, z, 0.05f), _p_start({x, y, z})
{
    _fireBox.setColor(RED);
    _fireBox.setIs_texture(false);
    _poolVector.push_back(std::vector<Vector3>());
    for (float yy = 0.2f; yy < 0.8f; yy += 0.1f)
    {
        for (float zz = 0.0f; zz < 1.0f; zz += 0.1f)
        {
            for (float xx = 0.0f; xx < 1.0f; xx += 0.1f)
            {
                _poolVector[0].push_back(Vector3{x + xx - 0.5f, y + yy - 0.5f, z + zz - 0.5f});
            }
        }
    }
    _poolVector.push_back(_poolVector[0]);
    _poolVector.push_back(_poolVector[0]);
    _poolVector.push_back(_poolVector[0]);
    _position_explosion.push_back(Vector3 {x - 0.5f + 1.0f, y - 0.5f, z - 0.5f});
    _position_explosion.push_back(Vector3 {x - 0.5f, y - 0.5f, z - 0.5f});
    _position_explosion.push_back(Vector3 {x - 0.5f, y - 0.5f, z + 1.0f - 0.5f});
    _position_explosion.push_back(Vector3 {x - 0.5f, y - 0.5f, z - 0.5f});
    _range.push_back(range + 0.3f + _p_start.x);
    _range.push_back(_p_start.x - (range + 0.3f));
    _range.push_back(_p_start.z + range + 0.3f);
    _range.push_back(_p_start.z - (range + 0.3f));
}

BoxPool::~BoxPool()
{
}

void BoxPool::addRangeVectorExplosion(Vector3 &a, float z, float x, int index)
{
    for (float yy = 0.2f; yy < 0.8f; yy += 0.1f)
    {
        for (float zz = 0.0f; zz < z; zz += 0.1f)
        {
            for (float xx = 0.0f; xx < x; xx += 0.1f)
            {
                _poolVector[index].push_back(Vector3{a.x + xx, a.y + yy, a.z + zz});
            }
        }
    }
}

std::vector<float> BoxPool::getRangeAllDirection() const
{
    return (_range);
}

void BoxPool::moveExplosion(Game::Map *map, std::vector<Vector3> &_p_chara, std::vector<Game::Character*> &chara)
{
    Game::TYPE_BLOCK tmp;

    if (_poolVector.size() == 4)
    {
        if (_position_explosion[0].x + 0.1f < _range[0]) {
            tmp = map->getTypeBlock(round(_position_explosion[0].z), round(_position_explosion[0].x + 0.1f));
            if (tmp != Game::TYPE_BLOCK::WALL_BLOCK && tmp != Game::TYPE_BLOCK::UNBREAKABLE_BLOCK) {
                addRangeVectorExplosion(_position_explosion[0], 1.0f, 0.1f, 0);
                _position_explosion[0].x += 0.1f;
            }
            if (tmp == Game::TYPE_BLOCK::BREAKABLE_BLOCK) {
                map->breakWall(round(_position_explosion[0].z), round(_position_explosion[0].x));
                _range[0] = round(_position_explosion[0].x) + 0.5;
                map->RandomAddPowerUp(round(_position_explosion[0].x), round(_position_explosion[0].y + 0.5), round(_position_explosion[0].z));
            }
        }

        if (_position_explosion[1].x - 0.1f > _range[1])
        {
            tmp = map->getTypeBlock(round(_position_explosion[1].z), round(_position_explosion[1].x - 0.1f));
            if (tmp != Game::TYPE_BLOCK::WALL_BLOCK && tmp != Game::TYPE_BLOCK::UNBREAKABLE_BLOCK) {
                _position_explosion[1].x -= 0.1f;
                addRangeVectorExplosion(_position_explosion[1], 1.0f, 0.1f, 1);
            }
            if (tmp == Game::TYPE_BLOCK::BREAKABLE_BLOCK) {
                map->breakWall(round(_position_explosion[1].z), round(_position_explosion[1].x));
                _range[1] = round(_position_explosion[1].x) - 0.5;
                map->RandomAddPowerUp(round(_position_explosion[1].x), round(_position_explosion[1].y + 0.5), round(_position_explosion[1].z));
            }
        }

        if (_position_explosion[2].z + 0.1f < _range[2])
        {
            tmp = map->getTypeBlock(round(_position_explosion[2].z + 0.1f), round(_position_explosion[2].x));
            if (tmp != Game::TYPE_BLOCK::WALL_BLOCK && tmp != Game::TYPE_BLOCK::UNBREAKABLE_BLOCK) {
                addRangeVectorExplosion(_position_explosion[2], 0.1f, 1.0f, 2);
                _position_explosion[2].z += 0.1f;
            }
            if (tmp == Game::TYPE_BLOCK::BREAKABLE_BLOCK) {
                map->breakWall(round(_position_explosion[2].z), round(_position_explosion[2].x));
                _range[2] = round(_position_explosion[2].z) + 0.5;
                map->RandomAddPowerUp(round(_position_explosion[2].x), round(_position_explosion[2].y + 0.5), round(_position_explosion[2].z));
            }
        }

        if (_position_explosion[3].z - 0.1f > _range[3])
        {
            tmp = map->getTypeBlock(round(_position_explosion[3].z - 0.1f), round(_position_explosion[3].x));
            if (tmp != Game::TYPE_BLOCK::WALL_BLOCK && tmp != Game::TYPE_BLOCK::UNBREAKABLE_BLOCK) {
                _position_explosion[3].z -= 0.1f;
                addRangeVectorExplosion(_position_explosion[3], 0.1f, 1.0f, 3);
            }
            if (tmp == Game::TYPE_BLOCK::BREAKABLE_BLOCK) {
                map->breakWall(round(_position_explosion[3].z), round(_position_explosion[3].x));
                _range[3] = round(_position_explosion[3].z) - 0.5;
                map->RandomAddPowerUp(round(_position_explosion[3].x), round(_position_explosion[3].y + 0.5), round(_position_explosion[3].z));
            }
        }
        for (int i = 0; i != _p_chara.size(); i++) {
            if (!chara[i]->getLife())
                continue;
            if (round(_p_chara[i].z) == round(_p_start.z) && round(_p_chara[i].x) == round(_p_start.x)) {
                chara[i]->decrementLife();
            }
            for (int a = 0; a < 4; a++) {
                if (round(_p_chara[i].z) == round(_position_explosion[a].z) && round(_p_chara[i].x) == round(_position_explosion[a].x)) {
                    chara[i]->decrementLife();
                }
            }
        }
    }
}

void BoxPool::drawExplosion() const
{
    Vector3 p;

    for (int a = 0; a < _poolVector.size(); a++) {
        for (int i = 0; i != _poolVector[a].size(); i++)
        {
            _fireBox.drawAtPosition(_poolVector[a][i]);
        }
    }
}
