/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** BombePool
*/

#include "BombPool.hpp"

Game::BombPool::BombPool()
{
}

Game::BombPool::~BombPool()
{
}

void Game::BombPool::initPLayerBomb(int nb)
{
    for (int i = 0; i != nb; i++) {
        _bomb_range_player[i].first = 1;
        _bomb_range_player[i].second = 1;
    }
}

void Game::BombPool::addBombPlayer(int _id_player)
{
    std::map<int, std::pair<int, int>>::iterator it = _bomb_range_player.find(_id_player);

    if (it != _bomb_range_player.end()) {
        it->second.first += 1;
    }
    else {
        _bomb_range_player[_id_player].first = 1;
    }
}

int Game::BombPool::getBombPlayer(int _id_player)
{
    std::map<int, std::pair<int, int>>::iterator it = _bomb_range_player.find(_id_player);

    if (it != _bomb_range_player.end())
        return (it->second.first);
    return (0);
}

void Game::BombPool::setBombPlayer(int _id_player, int nb)
{
    std::map<int, std::pair<int, int>>::iterator it = _bomb_range_player.find(_id_player);

    if (it != _bomb_range_player.end()) {
        it->second.first = nb;
    }
    else {
        _bomb_range_player[_id_player].first = nb;
    }
}

void Game::BombPool::addRangePlayer(int _id_player)
{
    std::map<int, std::pair<int, int>>::iterator it = _bomb_range_player.find(_id_player);

    if (it != _bomb_range_player.end()) {
        it->second.second += 1;
    }
    else {
        _bomb_range_player[_id_player].second = 1;
    }
}

void Game::BombPool::SetRangePlayer(int _id_player, int range)
{
    std::map<int, std::pair<int, int>>::iterator it = _bomb_range_player.find(_id_player);

    if (it != _bomb_range_player.end()) {
        it->second.second = range;
    }
    else {
        _bomb_range_player[_id_player].second = range;
    }
}

bool Game::BombPool::dropBomb(Vector3 position, int _id_player)
{
    std::map<int, std::pair<int, int>>::iterator it = _bomb_range_player.find(_id_player);

    if (it != _bomb_range_player.end() && it->second.first > 0) {
        _bombs.push_back(Game::Bomb(round(position.x), round(position.y), round(position.z), _id_player, it->second.second));
        it->second.first--;
        return true;
    }
    return false;
}

void Game::BombPool::drawBomb(void)
{
    if (_bombs.size() > 0) {
        for (size_t i = 0; i < _bombs.size(); i++)
            _bombs[i].drawBombe(_fxbomb);
    }
}

void Game::BombPool::checkBomb(void)
{
    for (size_t i = 0; i < _bombs.size(); i++) {
        if (_bombs[i].checkTime()) {
            _bomb_range_player[_bombs[i].getIdPlayer()].first += 1;
            _bombs.erase(_bombs.begin() + i);
            i--;
        }
    }
}

void Game::BombPool::moveExplosion(Game::Map *map, std::vector<Vector3> &_p_chara, std::vector<Character*> &chara)
{
    if (_bombs.size() > 0) {
        for (size_t i = 0; i < _bombs.size(); i++)
            if (_bombs[i].getTimerMillisecond(std::chrono::steady_clock::now()) > std::chrono::milliseconds(1500))
                _bombs[i].moveExplosion(map, _p_chara, chara);
    }
}
