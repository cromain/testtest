/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Velocity
*/

#include "Velocity.hpp"

Velocity::Velocity()
:_v(0.01)
{
}

Velocity::~Velocity()
{
}

float Velocity::getVelocity() const
{
    return (_v);
}

void Velocity::setVelocity(float v)
{
    _v = v;
}