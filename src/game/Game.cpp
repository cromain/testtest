/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Game
*/

#include "Game.hpp"

Game::Game::Game()
: _currentScreen(MENU)
{

    _engine = new Engine::Engine();
    _screens.push_back(new Screen::Menu);
    _screens.push_back(new Screen::Lobby);
    _screens.push_back(nullptr);
    _screens.push_back(new Screen::Settings);
    _soundMenu = new Screen::SoundMenu();
    _sound_m = false;
}

Game::Game::~Game()
{

}

void Game::Game::handleEvent()
{
    if (_sound_m == true) {
        _soundMenu->handleMouse((int *) &_currentScreen);
    }

    if (_currentScreen == LOBBY) {
        _screens[_currentScreen]->handleMouse((int *) &_currentScreen);
        _screens[_currentScreen]->handleKeyboard((int *) &_currentScreen, &_sound_m);
        if (_currentScreen == GAMEPLAY) {
            if (_screens[2] != nullptr) {
               ((Screen::Gameplay*) _screens[2])->~Gameplay();
               _screens.emplace(_screens.begin(), nullptr);
            }
            _engine->beginDraw();
            DrawText("LOADING DATA...", 640, 110, 40, DARKBLUE);
            DrawRectangle(300, 200, 0, 60, SKYBLUE);
            DrawRectangleLines(300, 200, 1000, 60, DARKGRAY);
            _engine->endDraw();
            _screens[2] = new Screen::Gameplay();
            ((Screen::Gameplay*) _screens[GAMEPLAY])->initGame(((Screen::Lobby*) _screens[LOBBY])->prealoadGame());
            std::cout << std::endl;
            std::cout << std::endl;
        }
    }
    else {
        _screens[_currentScreen]->handleMouse((int *) &_currentScreen);
        _screens[_currentScreen]->handleKeyboard((int *) &_currentScreen, &_sound_m);
    }
}

void Game::Game::displayScreen()
{
    _engine->beginDraw();

    if (_sound_m == true)
        _soundMenu->drawScreen();
    else
        _screens[_currentScreen]->drawScreen();

    _engine->endDraw();
}