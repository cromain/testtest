/*
** EPITECH PROJECT, 2021
** Map.cpp
** File description:
** Map
*/

#include "Map.hpp"

Game::Map::Map(int width, int height)
:PowerUp(), _width(width), _height(height), _sol(Game::Block(0.0f, 0.0f, 0.0f, 1.0f, Game::TYPE_BLOCK::EMPTY_BLOCK))
{
    init();
}

Game::Map::~Map()
{
    for (int i = 0; i < 5; i++) {
        _model_texture[i].unloadModel();
    }
}

void Game::Map::fillMap(void)
{
    Game::Block block(0.0f, 0.0f, 0.0f, 1.5f, Game::TYPE_BLOCK::EMPTY_BLOCK);
    float size_block = 1;

    _map.push_back(std::vector<Game::Block>());
    for (float x = 0; x < _width + 2; x += 1) {
        block.setPosition(x, 0.0f , 0);
        _map[0].push_back(block);
        _map[0][x].setType(Game::TYPE_BLOCK::WALL_BLOCK);
        _map[0][x].setSize(size_block);
    }
    for (float y = 1; y < _height + 1; y += 1) {
        _map.push_back(std::vector<Game::Block>());
        block.setPosition(0, 0.0f , y);
        _map[(int) y].push_back(block);
        _map[y][0].setType(Game::TYPE_BLOCK::WALL_BLOCK);
        _map[y][0].setSize(size_block);
        for (float x = 1; x < _width + 1; x += 1) {
            block.setPosition(x, 0.0f , y);
            _map[(int) y].push_back(block);
            if ((int) x % 2 == 0 && (int) y % 2 == 0) {
                _map[y][x].setType(Game::TYPE_BLOCK::UNBREAKABLE_BLOCK);
                _map[y][x].setHeight(size_block);
            }
        }
        block.setPosition(_width + 1, 0.0f , y);
        _map[(int) y].push_back(block);
        _map[y][_width + 1].setType(Game::TYPE_BLOCK::WALL_BLOCK);
        _map[y][_width + 1].setSize(size_block);
    }
    _map.push_back(std::vector<Game::Block>());
    for (float x = 0; x < _width + 2; x += 1) {
        block.setPosition(x, 0.0f , _height + 1);
        _map[_height + 1].push_back(block);
        _map[_height + 1][x].setType(Game::TYPE_BLOCK::WALL_BLOCK);
        _map[_height + 1][x].setSize(size_block);
    }
}

void Game::Map::init()
{
    fillMap();
    generation();
    freeSpaceForPlayer();
    setModelBoxes();
}

void Game::Map::setModelBoxes()
{
    const char *a[5] = {"../ressources/models/blockMoving.obj", "../ressources/models/crate.obj", "../ressources/models/crateStrong.obj", "../ressources/models/blockMoving.obj", "../ressources/models/block.obj"};

    for (int i = 0; i < 5; i++) {
        _model_texture.push_back(Game::Block(0.0f, 0.0f, 0.0f, 1, Game::TYPE_BLOCK(i)));
        _model_texture[i].loadModel(a[i]);
    }
    _sol.setModel(_model_texture[_sol.getType()].getModel());
    for (float y = 0; y < _map.size(); y += 1) {
        for (float x = 0; x < _map[y].size(); x += 1) {
            _map[y][x].setModel(_model_texture[_map[y][x].getType()].getModel());
        }
    }
}

void Game::Map::generation()
{
    int nb_block_to_change = ((_width - 1) * (_height - 1)) / 3;
    float x, y;

    for (int i = 0; i != nb_block_to_change; i++) {
        x = std::rand() % _width;
        y = std::rand() % _height;
        while ((int) x % 2 != 0 && (int) y % 2 != 0) {
            x = std::rand() % _width;
            y = std::rand() % _height;
        }
        if (_map[y + 1][x + 1].getType() == Game::TYPE_BLOCK::EMPTY_BLOCK) {
            _map[y + 1][x + 1].setType(Game::TYPE_BLOCK::BREAKABLE_BLOCK);
        }
        else {
            i--;
        }
    }
}

void Game::Map::freeSpaceForPlayer(void)
{
    _map[1][1].setType(Game::TYPE_BLOCK::PLAYER_BLOCK);
    _map[1][2].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[2][1].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[1][_width].setType(Game::TYPE_BLOCK::PLAYER_BLOCK);
    _map[2][_width].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[1][_width - 1].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[_height][1].setType(Game::TYPE_BLOCK::PLAYER_BLOCK);
    _map[_height][2].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[_height - 1][1].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[_height][_width].setType(Game::TYPE_BLOCK::PLAYER_BLOCK);
    _map[_height - 1][_width].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
    _map[_height][_width - 1].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
}

void Game::Map::Display()
{
    Vector3 a;

    for (float y = 0; y < _map.size(); y += 1) {
        for (float x = 0; x < _map[y].size(); x += 1) {
            a = _map[y][x].getPosition();
            a.y -= 0.3;
            _sol.setPositionVector(a);
            _sol.draw();
            if (_map[y][x].getType() != Game::TYPE_BLOCK::EMPTY_BLOCK && _map[y][x].getType() != Game::TYPE_BLOCK::PLAYER_BLOCK)
                _map[y][x].draw();
        }
    }
    drawPowerUp();
}

std::vector<std::vector<Game::Block>> &Game::Map::getMap(void)
{
    return _map;
}

int Game::Map::getWidth(void) const
{
    return _width;
}

int Game::Map::getHeight(void) const
{
    return _height;
}

Game::TYPE_BLOCK Game::Map::getTypeBlock(int z, int x) const
{
    return (_map[z][x].getType());
}

void Game::Map::breakWall(int z, int x)
{
    _map[z][x].setType(Game::TYPE_BLOCK::EMPTY_BLOCK);
}

