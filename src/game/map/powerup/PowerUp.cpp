/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** PowerUp
*/

#include "PowerUp.hpp"

Game::PowerUp::PowerUp()
{
    const char *a[4] = {"../ressources/textures/bonus_bomb_up.png", "../ressources/textures/bonus_fire.png", "../ressources/textures/bonus_skate.png", "../ressources/textures/bonus_soft_block_pass.png"};
    
    for (int i = 0; i != 4; i++)
        _powerUpTexture.push_back(Engine::TextureClass(a[i]));
}

Game::PowerUp::~PowerUp()
{
}

void Game::PowerUp::RandomAddPowerUp(float x, float y, float z)
{
    int i = std::rand() % 10;

    if (i < 4) {
        AddPowerUp(x, y, z, i);
    }
}

void Game::PowerUp::AddPowerUp(float x, float y, float z, int type)
{
    _powerUps.push_back(Engine::Square(x, y, z, 0.8f, _powerUpTexture[type].getTexture()));
    _t.push_back(type);
}

std::vector<Engine::Square> &Game::PowerUp::getPowerUp()
{
    return _powerUps;
}

void Game::PowerUp::drawPowerUp() const
{
    for (int i = 0; i < _powerUps.size(); i++) {
        _powerUps[i].draw();
    }
}

int Game::PowerUp::isOnBonus(float z, float x)
{
    int t = 0;
    for (int i = 0; i < _powerUps.size(); i++) {
        if (round(_powerUps[i].getPosition().x) == round(x) &&  round(_powerUps[i].getPosition().z) == round(z)) {
            _powerUps.erase(_powerUps.begin() + i);
            t = _t[i];
            _t.erase(_t.begin() + i);
            return (_t[i]);
        }
    }
    return (-1);
}
