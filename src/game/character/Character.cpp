/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Character
*/

#include "Character.hpp"

Game::Character::Character(int id, Vector3 &p, Vector3 &z, int *load, bool bot) : Animation(id, load), Velocity(), Life(), _id(id), _position(p), _posbomb(z), _color(BLACK), character(bot)
{
    switch (id) {
        case 1:
            _yaw = 0.0f;
            _scale = 1.2f;
            _moveup = 0;
            _moveleft = 0;
            _movedown = 0;
            _moverigth = 0;
            break;
        case 2:
            _yaw = 0.0f;
            _scale = 1.2f;
            _moveup = 0;
            _moveleft = 0;
            _movedown = 0;
            _moverigth = 0;
            break;
        case 3:
            _yaw = 180.0f;
            _scale = 1.2f;
            _moveup = 0;
            _moveleft = 0;
            _movedown = 0;
            _moverigth = 0;
            break;
        case 4:
            _yaw = 180.0f;
            _scale = 1.2f;
            _moveup = 0;
            _moveleft = 0;
            _movedown = 0;
            _moverigth = 0;
            break;
    }
}

Game::Character::~Character()
{
}

void Game::Character::draw(int bomb)
{
    if (getLife()) {
        speed++;
        if (IsKeyDown(KEY_SPACE) && drop == false && bomb > 0) {
            drop = true;
        }
        if (drop == false && frame < 34) {
            DrawModel(vecModel[frame], _position, _scale, WHITE);
        }
        else if (drop == false) {
            frame = 0;
            speedDrop = 0;
        }
        if (drop == false && !IsKeyDown(KEY_UP) && !IsKeyDown(KEY_DOWN) && !IsKeyDown(KEY_LEFT) && !IsKeyDown(KEY_RIGHT) && !IsKeyDown(KEY_SPACE))
        {
            frame = 0;
            speed = 0;
            speedDrop = 0;
        }
        if (drop == false && IsKeyDown(KEY_UP) || IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_RIGHT))
        {
            if (speed % 2 == 0)
                frame++;
        }
        if (drop == true) {
            DrawModel(vecModelDrop[speedDrop], _position, _scale, WHITE);
            speedDrop++;
        }
        if (speedDrop >= 37) {
            speedDrop = 0;
            drop = false;
        }
        if (drop == false && bomb > 0)
            DrawSphere(_posbomb, 0.25, _color);
    }
}

void Game::Character::draw1(int bomb)
{
    if (getLife()) {
        speed++;
        if (IsKeyDown(KEY_Q) && drop == false && bomb > 0) {
            drop = true;
        }
        if (drop == false && frame < 34) {
            DrawModel(vecModel[frame], _position, _scale, WHITE);
        }
        else if (drop == false) {
            frame = 0;
            speedDrop = 0;
        }
        if (drop == false && !IsKeyDown(KEY_W) && !IsKeyDown(KEY_S) && !IsKeyDown(KEY_A) && !IsKeyDown(KEY_D) && !IsKeyDown(KEY_Q))
        {
            frame = 0;
            speed = 0;
            speedDrop = 0;
        }
        if (drop == false && IsKeyDown(KEY_W) || IsKeyDown(KEY_S) || IsKeyDown(KEY_A) || IsKeyDown(KEY_D))
        {
            if (speed % 2 == 0)
                frame++;
        }
        if (drop == true) {
            DrawModel(vecModelDrop[speedDrop], _position, _scale, WHITE);
            speedDrop++;
        }
        if (speedDrop >= 37) {
            speedDrop = 0;
            drop = false;
        }
        if (drop == false && bomb > 0)
            DrawSphere(_posbomb, 0.25, _color);
    }
}

void Game::Character::draw2(int bomb)
{
    if (getLife()) {
        speed++;
        if (IsKeyDown(KEY_U) && drop == false && bomb > 0) {
            drop = true;
        }
        if (drop == false && frame < 34) {
            DrawModel(vecModel[frame], _position, _scale, WHITE);
        }
        else if (drop == false) {
            frame = 0;
            speedDrop = 0;
        }
        if (drop == false && !IsKeyDown(KEY_I) && !IsKeyDown(KEY_K) && !IsKeyDown(KEY_J) && !IsKeyDown(KEY_L) && !IsKeyDown(KEY_U))
        {
            frame = 0;
            speed = 0;
            speedDrop = 0;
        }
        if (drop == false && IsKeyDown(KEY_I) || IsKeyDown(KEY_J) || IsKeyDown(KEY_K) || IsKeyDown(KEY_L))
        {
            if (speed % 2 == 0)
                frame++;
        }
        if (drop == true) {
            DrawModel(vecModelDrop[speedDrop], _position, _scale, WHITE);
            speedDrop++;
        }
        if (speedDrop >= 37) {
            speedDrop = 0;
            drop = false;
        }
        if (drop == false && bomb > 0)
            DrawSphere(_posbomb, 0.25, _color);
    }
}

void Game::Character::move(Game::Map *map)
{
    int u = 0;
    bool collision = false;
    bool up = false;
    bool down = false;
    bool right = false;
    bool left = false;

    if (_id != 2 || getLife() == 0)
        return;
    if (drop == false && IsKeyDown(KEY_UP) && down == false && right == false && left == false) {
        _moveup++;
        _moveleft = 0;
        _movedown = 0;
        _moverigth = 0;
        up = true;
        _position.z -= getVelocity();
        _posbomb.z -= getVelocity();

        if (willColide(map->getMap(), _position) == true) {
            _position.z += getVelocity();
             _posbomb.z += getVelocity();
        }
        _yaw = 180.0f;
    }
    if (_moveup == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.z -= 0.6f;
    }
    if (drop == false && IsKeyDown(KEY_DOWN) && up == false && right == false && left == false) {
        _movedown++;
        _moveup = 0;
        _moveleft = 0;
        _moverigth = 0;
        down = true;
        _position.z += getVelocity();
        _posbomb.z += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.z -= getVelocity();
            _position.z -= getVelocity();
        }
        _yaw = 0.0f;
    }
    if (_movedown == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
    }
    if (drop == false && IsKeyDown(KEY_LEFT) && down == false && right == false && up == false) {
        _moveleft++;
        _moveup = 0;
        _movedown = 0;
        _moverigth = 0;
        left = true;
        _position.x -= getVelocity();
        _posbomb.x -= getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x += getVelocity();
            _position.x += getVelocity();
        }
        _yaw = 90.0f;
    }
    if (_moveleft == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x -= 0.25f;
        _posbomb.z -= 0.45f;
    }
    if (drop == false && IsKeyDown(KEY_RIGHT) && down == false && up == false && left == false) {
        _moverigth++;
        _moveup = 0;
        _moveleft = 0;
        _movedown = 0;
        right = true;
        _position.x += getVelocity();
        _posbomb.x += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x -= getVelocity();
            _position.x -= getVelocity();
        }
        _yaw = -90.0f;
    }
    if (_moverigth == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x += 0.2f;
         _posbomb.z -= 0.45f;
    }
    for (int i = 0; i <= 39; i++)
        vecModel[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
    for (int i = 0; i <= 38; i++)
        vecModelDrop[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
}

void Game::Character::move2(Game::Map *map)
{
    int u = 0;
    bool collision = false;
    bool up = false;
    bool down = false;
    bool right = false;
    bool left = false;

    if (getLife() == 0)
        return;
    if (drop == false && IsKeyDown(KEY_W) && down == false && right == false && left == false) {
        _moveup++;
        _moveleft = 0;
        _movedown = 0;
        _moverigth = 0;
        up = true;
        _position.z -= getVelocity();
        _posbomb.z -= getVelocity();

        if (willColide(map->getMap(), _position) == true) {
            _position.z += getVelocity();
             _posbomb.z += getVelocity();
        }
        _yaw = 180.0f;
    }
    if (_moveup == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.z -= 0.6f;
    }
    if (drop == false && IsKeyDown(KEY_S) && up == false && right == false && left == false) {
        _movedown++;
        _moveup = 0;
        _moveleft = 0;
        _moverigth = 0;
        down = true;
        _position.z += getVelocity();
        _posbomb.z += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.z -= getVelocity();
            _position.z -= getVelocity();
        }
        _yaw = 0.0f;
    }
    if (_movedown == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
    }
    if (drop == false && IsKeyDown(KEY_A) && down == false && right == false && up == false) {
        _moveleft++;
        _moveup = 0;
        _movedown = 0;
        _moverigth = 0;
        left = true;
        _position.x -= getVelocity();
        _posbomb.x -= getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x += getVelocity();
            _position.x += getVelocity();
        }
        _yaw = 90.0f;
    }
    if (_moveleft == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x -= 0.25f;
        _posbomb.z -= 0.45f;
    }
    if (drop == false && IsKeyDown(KEY_D) && down == false && up == false && left == false) {
        _moverigth++;
        _moveup = 0;
        _moveleft = 0;
        _movedown = 0;
        right = true;
        _position.x += getVelocity();
        _posbomb.x += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x -= getVelocity();
            _position.x -= getVelocity();
        }
        _yaw = -90.0f;
    }
    if (_moverigth == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x += 0.2f;
         _posbomb.z -= 0.45f;
    }
    for (int i = 0; i <= 39; i++)
        vecModel[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
    for (int i = 0; i <= 38; i++)
        vecModelDrop[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
}

void Game::Character::move3(Game::Map *map)
{
    int u = 0;
    bool collision = false;
    bool up = false;
    bool down = false;
    bool right = false;
    bool left = false;

    if (getLife() == 0)
        return;
    if (drop == false && IsKeyDown(KEY_I) && down == false && right == false && left == false) {
        _moveup++;
        _moveleft = 0;
        _movedown = 0;
        _moverigth = 0;
        up = true;
        _position.z -= getVelocity();
        _posbomb.z -= getVelocity();

        if (willColide(map->getMap(), _position) == true) {
            _position.z += getVelocity();
             _posbomb.z += getVelocity();
        }
        _yaw = 180.0f;
    }
    if (_moveup == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.z -= 0.6f;
    }
    if (drop == false && IsKeyDown(KEY_K) && up == false && right == false && left == false) {
        _movedown++;
        _moveup = 0;
        _moveleft = 0;
        _moverigth = 0;
        down = true;
        _position.z += getVelocity();
        _posbomb.z += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.z -= getVelocity();
            _position.z -= getVelocity();
        }
        _yaw = 0.0f;
    }
    if (_movedown == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
    }
    if (drop == false && IsKeyDown(KEY_J) && down == false && right == false && up == false) {
        _moveleft++;
        _moveup = 0;
        _movedown = 0;
        _moverigth = 0;
        left = true;
        _position.x -= getVelocity();
        _posbomb.x -= getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x += getVelocity();
            _position.x += getVelocity();
        }
        _yaw = 90.0f;
    }
    if (_moveleft == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x -= 0.25f;
        _posbomb.z -= 0.45f;
    }
    if (drop == false && IsKeyDown(KEY_L) && down == false && up == false && left == false) {
        _moverigth++;
        _moveup = 0;
        _moveleft = 0;
        _movedown = 0;
        right = true;
        _position.x += getVelocity();
        _posbomb.x += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x -= getVelocity();
            _position.x -= getVelocity();
        }
        _yaw = -90.0f;
    }
    if (_moverigth == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x += 0.2f;
         _posbomb.z -= 0.45f;
    }
    for (int i = 0; i <= 39; i++)
        vecModel[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
    for (int i = 0; i <= 38; i++)
        vecModelDrop[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
}

void Game::Character::move4(Game::Map *map)
{
    int u = 0;
    bool collision = false;
    bool up = false;
    bool down = false;
    bool right = false;
    bool left = false;

    if (getLife() == 0)
        return;
    if (drop == false && IsKeyDown(KEY_W) && down == false && right == false && left == false) {
        _moveup++;
        _moveleft = 0;
        _movedown = 0;
        _moverigth = 0;
        up = true;
        _position.z -= getVelocity();
        _posbomb.z -= getVelocity();

        if (willColide(map->getMap(), _position) == true) {
            _position.z += getVelocity();
             _posbomb.z += getVelocity();
        }
        _yaw = 180.0f;
    }
    if (_moveup == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.z -= 0.6f;
    }
    if (drop == false && IsKeyDown(KEY_S) && up == false && right == false && left == false) {
        _movedown++;
        _moveup = 0;
        _moveleft = 0;
        _moverigth = 0;
        down = true;
        _position.z += getVelocity();
        _posbomb.z += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.z -= getVelocity();
            _position.z -= getVelocity();
        }
        _yaw = 0.0f;
    }
    if (_movedown == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
    }
    if (drop == false && IsKeyDown(KEY_A) && down == false && right == false && up == false) {
        _moveleft++;
        _moveup = 0;
        _movedown = 0;
        _moverigth = 0;
        left = true;
        _position.x -= getVelocity();
        _posbomb.x -= getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x += getVelocity();
            _position.x += getVelocity();
        }
        _yaw = 90.0f;
    }
    if (_moveleft == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x -= 0.25f;
        _posbomb.z -= 0.45f;
    }
    if (drop == false && IsKeyDown(KEY_D) && down == false && up == false && left == false) {
        _moverigth++;
        _moveup = 0;
        _moveleft = 0;
        _movedown = 0;
        right = true;
        _position.x += getVelocity();
        _posbomb.x += getVelocity();
        if (willColide(map->getMap(), _position) == true) {
            _posbomb.x -= getVelocity();
            _position.x -= getVelocity();
        }
        _yaw = -90.0f;
    }
    if (_moverigth == 1) {
        _posbomb = {_position.x, _position.y + 1.4f, _position.z + 0.5f};
        _posbomb.x += 0.2f;
         _posbomb.z -= 0.45f;
    }
    for (int i = 0; i <= 39; i++)
        vecModel[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
    for (int i = 0; i <= 38; i++)
        vecModelDrop[i].transform = MatrixRotateXYZ((Vector3){ 0.0f, DEG2RAD * _yaw, 0.0f });
}

bool Game::Character::checkIfDropBombe()
{
    if (getLife() == 0)
        return false;
    if (speedDrop == 14) {
        return (true);
    }
    return false;
}

bool Game::Character::willColide(std::vector<std::vector<Game::Block>> map, Vector3 pos)
{
        for (std::size_t y = 0; y < map[round(pos.z)].size(); y++) {
            if (map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::EMPTY_BLOCK && map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::PLAYER_BLOCK) {
                if (map[round(pos.z)][y].getPosition().x == round(pos.x)) {
                    return true;
                }
            }
        }
    return false;
}


void Game::Character::setPosition(float x, float y, float z)
{
    _position  = {x, y, z};
    _posbomb = {x + 0.5f, y + 1.2f, z};
}

