/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Bot
*/

#include "Bot.hpp"

Game::Bot::~Bot()
{
}

bool Game::Bot::isInBombRange(float z, float x)
{
    Vector3 positionBombe;
    std::vector<float> range;

    for (std::size_t i = 0; i < _bombs.size(); i++)
    {
        range = _bombs[i].getRangeAllDirection();
        positionBombe = _bombs[i].getPosition();
        if (z > range[3] - 0.8f && z < range[2] + 0.8f && (x > positionBombe.x - 0.8f && x < positionBombe.x + 0.8f))
        {

            return true;
        }
        if (x > range[1] - 0.8f && x < range[0] + 0.8f && (z > positionBombe.z  - 0.8f && z < positionBombe.z + 0.8f))
        {
            return true;
        }
    }
    return (false);
}

bool Game::Bot::isInBombRange(void)
{
    Vector3 positionBombe;
    std::vector<float> range;

    for (std::size_t i = 0; i < _bombs.size(); i++)
    {
        range = _bombs[i].getRangeAllDirection();
        positionBombe = _bombs[i].getPosition();
        if (_position.z > range[3] - 0.8f && _position.z < range[2] + 0.8f && (_position.x > positionBombe.x - 0.8f && _position.x < positionBombe.x + 0.8f))
        {
            return true;
        }
        if (_position.x > range[1] - 0.8f && _position.x < range[0] + 0.8f && (_position.z > positionBombe.z  - 0.8f && _position.z < positionBombe.z + 0.8f))
        {
            return true;
        }
    }
    return (false);
}

bool chackIsInVector(std::vector<Vector3> &pos, float z, float x)
{
    for (int i = 0; i != pos.size(); i++) {
        if (pos[i].x == x && pos[i].z == z)
            return (true);
    }
    return false;
}

std::vector<Vector3> Game::Bot::checkWay(std::vector<Vector3> pos, Game::Map *map)
{
    std::vector<Vector3> pos2;
    Vector3 tmp = pos[pos.size() - 1];
    Vector3 tmp2;

    // std::cout << tmp.z << " " << tmp.x <<"\n";
    if (isInBombRange(tmp.z, tmp.x))
    {
        if (!chackIsInVector(pos, tmp.z, tmp.x + 1) && (map->getTypeBlock(tmp.z, tmp.x + 1) == Game::TYPE_BLOCK::EMPTY_BLOCK || map->getTypeBlock(tmp.z, tmp.x + 1) == Game::TYPE_BLOCK::PLAYER_BLOCK))
        {
            std::cout << ((map->getTypeBlock(tmp.z, tmp.x + 1) == 0) ? "EMPTY_BLOCK" : (map->getTypeBlock(tmp.z, tmp.x + 1) == 3) ? "PLAYER_BLOCK" : "wtffffff") << std::endl;
            pos2 = pos;
            pos2.push_back(Vector3{tmp.x + 1, tmp.y, tmp.z});
            pos2 = checkWay(pos2, map);
            if (isInBombRange(pos2.end()->z, pos2.end()->x))
            {
                pos2.clear();
            }
            else
            {
                return (pos2);
            }
        }
        if (!chackIsInVector(pos, tmp.z, tmp.x - 1) && (map->getTypeBlock(tmp.z, tmp.x - 1) == Game::TYPE_BLOCK::EMPTY_BLOCK || map->getTypeBlock(tmp.z, tmp.x - 1) == Game::TYPE_BLOCK::PLAYER_BLOCK))
        {
            // std::cout << ((map->getTypeBlock(tmp.z, tmp.x - 1) == 0) ? "EMPTY_BLOCK" : (map->getTypeBlock(tmp.z, tmp.x - 1) == 3) ? "PLAYER_BLOCK" : "wtffffff") << std::endl;
            pos2 = pos;
            pos2.push_back(Vector3{tmp.x - 1, tmp.y, tmp.z});
            pos2 = checkWay(pos2, map);
            if (isInBombRange(pos2.end()->z, pos2.end()->x))
            {
                pos2.clear();
            }
            else
            {
                return (pos2);
            }
        }
        if (!chackIsInVector(pos, tmp.z + 1, tmp.x) && (map->getTypeBlock(tmp.z + 1, tmp.x) == Game::TYPE_BLOCK::EMPTY_BLOCK || map->getTypeBlock(tmp.z + 1, tmp.x) == Game::TYPE_BLOCK::PLAYER_BLOCK))
        {
            // std::cout << ((map->getTypeBlock(tmp.z + 1, tmp.x) == 0) ? "EMPTY_BLOCK" : (map->getTypeBlock(tmp.z + 1, tmp.x) == 3) ? "PLAYER_BLOCK" : "wtffffff") << std::endl;
            pos2 = pos;
            pos2.push_back(Vector3{tmp.x, tmp.y, tmp.z + 1});
            pos2 = checkWay(pos2, map);
            if (isInBombRange(pos2.end()->z, pos2.end()->x))
            {
                pos2.clear();
            }
            else
            {
                return (pos2);
            }
        }
        if (!chackIsInVector(pos, tmp.z - 1, tmp.x) && (map->getTypeBlock(tmp.z - 1, tmp.x) == Game::TYPE_BLOCK::EMPTY_BLOCK || map->getTypeBlock(tmp.z - 1, tmp.x) == Game::TYPE_BLOCK::PLAYER_BLOCK))
        {
            // std::cout << ((map->getTypeBlock(tmp.z - 1, tmp.x) == 0) ? "EMPTY_BLOCK" : (map->getTypeBlock(tmp.z - 1, tmp.x) == 3) ? "PLAYER_BLOCK" : "wtffffff") << std::endl;
            pos2 = pos;
            pos2.push_back(Vector3{tmp.x, tmp.y, tmp.z - 1});
            pos2 = checkWay(pos2, map);
            if (isInBombRange(pos2.end()->z, pos2.end()->x))
            {
                pos2.clear();
            }
            else
            {
                return (pos2);
            }
        }
    }
    return pos;
}

void Game::Bot::getPathEscape(Game::Map *map)
{
    _target_escape.push_back(Vector3 {round(_position.x), round(_position.y), round(_position.z)});

    _target_escape = checkWay(_target_escape, map);
    _target_escape.erase(_target_escape.begin());
    std::cout << _target_escape.size() << std::endl;
}

Vector3 Game::Bot::chooseTarget(void)
{
    Vector3 res;
    Vector3 tmp;
    float distance = 0;
    float save = 0;
    for (std::size_t i = 0; i < _p_chara.size(); i++)
    {
        if (_p_chara[i].x == -1)
            continue;
        if (_id - 1 == i)
            continue;
        distance = sqrt((float)pow(_p_chara[i].z, 2) + (float)pow(_p_chara[i].x, 2));
        if (distance > save)
        {
            distance = save;
            res = _p_chara[i];
        }
    }
    return res;
}

bool Game::Bot::willColideWithBreakable(std::vector<std::vector<Game::Block>> map, Vector3 pos)
{
    for (std::size_t y = 0; y < map[round(pos.z)].size(); y++)
    {
        if (map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::EMPTY_BLOCK && map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::PLAYER_BLOCK, map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::BREAKABLE_BLOCK)
        {
            if (map[round(pos.z)][y].getPosition().x == round(pos.x))
            {
                return true;
            }
        }
    }
    return false;
}

void Game::Bot::moveAnotherDirection(Game::Map *map, bool up)
{
    if (up)
    {
        if (_target.z > _position.z)
        {
            if (willColide(map->getMap(), Vector3{_position.x, _position.y, _position.z + getVelocity()}))
            {
                if (!willColideWithBreakable(map->getMap(), Vector3{_position.x, _position.y, _position.z + getVelocity()}))
                {
                    _dropBomb = true;
                }
            }
            else
            {
                if (!_target_escape.size() && !isInBombRange(_position.z + getVelocity(), _position.x))
                    _position.z += getVelocity();
                else if (_target_escape.size())
                    _position.z += getVelocity();
            }
        }
        else
        {
            if (willColide(map->getMap(), Vector3{_position.x, _position.y, _position.z - getVelocity()}))
            {
                if (!willColideWithBreakable(map->getMap(), Vector3{_position.x, _position.y, _position.z - getVelocity()}))
                {
                    _dropBomb = true;
                }
            }
            else
            {
                if (!_target_escape.size() && !isInBombRange(_position.z - getVelocity(), _position.x))
                    _position.z -= getVelocity();
                else if (_target_escape.size())
                    _position.z -= getVelocity();
            }
        }
    }
    else
    {
        if (_target.x > _position.x)
        {
            if (willColide(map->getMap(), Vector3{_position.x + getVelocity(), _position.y, _position.z}))
            {
                if (!willColideWithBreakable(map->getMap(), Vector3{_position.x + getVelocity(), _position.y, _position.z}))
                {
                    _dropBomb = true;
                }
            }
            else
            {
                if (!_target_escape.size() && !isInBombRange(_position.z, _position.x + getVelocity()))
                    _position.x += getVelocity();
                else if (_target_escape.size())
                    _position.x += getVelocity();
            }
        }
        else
        {
            if (willColide(map->getMap(), Vector3{_position.x - getVelocity(), _position.y, _position.z}))
            {
                if (!willColideWithBreakable(map->getMap(), Vector3{_position.x - getVelocity(), _position.y, _position.z}))
                {
                    _dropBomb = true;
                }
            }
            else
            {
                if (!_target_escape.size() && !isInBombRange(_position.z, _position.x - getVelocity()))
                    _position.x -= getVelocity();
                else if (_target_escape.size())
                    _position.x -= getVelocity();
            }
        }
    }
}

void Game::Bot::goToTarget(Game::Map *map)
{
    if (abs(_target.z - _position.z) > abs(_target.x - _position.x))
        {
            if (_target.z > _position.z)
            {
                if (willColide(map->getMap(), Vector3{_position.x, _position.y, _position.z + getVelocity()}))
                {
                    if (!willColideWithBreakable(map->getMap(), Vector3{_position.x, _position.y, _position.z + getVelocity()}))
                    {
                        _dropBomb = true;
                    }
                    else
                    {
                        moveAnotherDirection(map, false);
                    }
                }
                else
                {
                    if (!_target_escape.size() && !isInBombRange(_position.z + getVelocity(), _position.x))
                        _position.z += getVelocity();
                    else if (_target_escape.size())
                        _position.z += getVelocity();
                }
            }
            else
            {
                if (willColide(map->getMap(), Vector3{_position.x, _position.y, _position.z - getVelocity()}))
                {
                    if (!willColideWithBreakable(map->getMap(), Vector3{_position.x, _position.y, _position.z - getVelocity()}))
                    {
                        _dropBomb = true;
                    }
                    else
                    {
                        moveAnotherDirection(map, false);
                    }
                }
                else
                {
                    if (!_target_escape.size() && !isInBombRange(_position.z - getVelocity(), _position.x))
                        _position.z -= getVelocity();
                    else if (_target_escape.size())
                        _position.z -= getVelocity();
                }
            }
        }
        else
        {
            if (_target.x > _position.x)
            {
                if (willColide(map->getMap(), Vector3{_position.x + getVelocity(), _position.y, _position.z}))
                {
                    if (!willColideWithBreakable(map->getMap(), Vector3{_position.x + getVelocity(), _position.y, _position.z}))
                    {
                        _dropBomb = true;
                    }
                    else
                    {
                        moveAnotherDirection(map, true);
                    }
                }
                else
                {
                    if (!_target_escape.size() && !isInBombRange(_position.z, _position.x + getVelocity()))
                        _position.x += getVelocity();
                    else if (_target_escape.size())
                        _position.x += getVelocity();
                }
            }
            else
            {
                if (willColide(map->getMap(), Vector3{_position.x - getVelocity(), _position.y, _position.z}))
                {
                    if (!willColideWithBreakable(map->getMap(), Vector3{_position.x - getVelocity(), _position.y, _position.z}))
                    {
                        _dropBomb = true;
                    }
                    else
                    {
                        moveAnotherDirection(map, true);
                    }
                }
                else
                {
                    if (!_target_escape.size() && !isInBombRange(_position.z, _position.x - getVelocity()))
                        _position.x -= getVelocity();
                    else if (_target_escape.size())
                        _position.x -= getVelocity();
                }
            }
        }
}

void Game::Bot::move(Game::Map *map)
{
    std::cout << "target" << _target.z << ":" << _target.x << std::endl;
    std::cout <<_position.z << ":" << _position.x << std::endl;
    std::string direction = "";
    if (isInBombRange())
    {
        if (_target_escape.size() && isInBombRange(_target_escape.end()->z, _target_escape.end()->x)) {
            _target_escape.clear();
            getPathEscape(map);
        }
        else if (!_target_escape.size()) {
            getPathEscape(map);
            if (_target_escape.size())
                _target = _target_escape[0];
        }
        if (_target_escape.size() && round(_position.x) == round(_target_escape[0].x) && round(_position.z) == round(_target_escape[0].z)) {
            if (_target_escape.size() == 1) {
                _target_escape.erase(_target_escape.begin());
                _target = chooseTarget();
            }
            else {
                _target_escape.erase(_target_escape.begin());
                _target = _target_escape[0];
            } 
        }
    }
    else {
        if (_target_escape.size())
            _target_escape.clear();
        _target = chooseTarget();
        if (_target.z > _position.z - 1 && _target.z < _position.z + 1 && _target.x > _position.x - 1 && _target.x < _position.x + 1) {
            _dropBomb = true;
            return;
        }
    }
    goToTarget(map);
}

bool Game::Bot::checkIfDropBombe()
{
    if (_dropBomb)
    {
        _dropBomb = false;
        return true;
    }
    else
    {
        return false;
    }
}


bool Game::Bot::willColide(std::vector<std::vector<Game::Block>> map, Vector3 pos)
{
        for (std::size_t y = 0; y < map[round(pos.z)].size(); y++) {
            if (map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::EMPTY_BLOCK && map[round(pos.z)][y].getType() != Game::TYPE_BLOCK::PLAYER_BLOCK) {
                for (int i = 0; i != _p_chara.size(); i++) {
                    if (i != _id - 1 && (round(pos.x) == round(_p_chara[i].x) && round(pos.z) == round(_p_chara[i].z)))
                        return true;
                }
                if (map[round(pos.z)][y].getPosition().x == round(pos.x)) {
                    return true;
                }
            }
        }
    return false;
}


