/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Bombe
*/

#include "Bomb.hpp"

Game::Bomb::Bomb(float x, float y, float z, int id_player, int range)
:BoxPool(x, y, z, range), Sphere(x, y + 0.2f, z, 0.4f), _id_player(id_player), _rangeSize(range)
{
    setColor(BLACK);
}

Game::Bomb::~Bomb()
{
    // CloseAudioDevice();
}

int Game::Bomb::getIdPlayer() const
{
    return _id_player;
}

bool Game::Bomb::checkTime()
{
    if (getTimerMillisecond(std::chrono::steady_clock::now()) > std::chrono::milliseconds(2500)) {
        return true;
    }
    return false;
}

void Game::Bomb::drawBombe(Sound sound) const
{
    if (getTimerMillisecond(std::chrono::steady_clock::now()) > std::chrono::milliseconds(1500)) {
        PlaySound(sound);
        drawExplosion();
    }
    else {
        draw();
    }
}

int Game::Bomb::getRange() const
{
    return _rangeSize;
}