/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Menu
*/

#include "Lobby.hpp"
#include <iostream>
#include "GameUI.hpp"

Screen::Lobby::Lobby()
{
    _logo = LoadTexture("../ressources/bombermanB.png");
    _background = LoadTexture("../ressources/menuB.jpg");
    _man = LoadTexture("../ressources/textures/lobbyskin/manlobby.png");
    _girl = LoadTexture("../ressources/textures/lobbyskin/girllobby.png");
    _font = LoadFont("../ressources/komika.png");
}

Screen::Lobby::~Lobby()
{
    UnloadTexture(_logo);
    UnloadTexture(_background);
    UnloadFont(_font);
}

std::vector<Game::GameUI> Screen::Lobby::prealoadGame() {

    float timePlayed = 0.0f;
    
    for (int i = 0; i != 4; i++)
    {
        timePlayed++;
        bool isPlayer = false;
        for (size_t y = 0; y != _players.size(); y++) {
            if (i == _players[y])
                isPlayer = true;
        }
        if (isPlayer) {
            Game::GameUI ui(std::string("Player" + std::to_string(i + 1)).c_str());
            ui.isPlayer(isPlayer);
            _stat.push_back(ui);
        }
        else {
            Game::GameUI ui(std::string("AI" + std::to_string(i + 1)).c_str());
            ui.isPlayer(isPlayer);
            _stat.push_back(ui);
        }
    }
    _stat[0].posUI(50.0f , 300.0f);
    _stat[1].posUI(((float)(GetScreenWidth() - 200)) , 300.0f);
    _stat[2].posUI(50.0f , 480.0f);
    _stat[3].posUI(((float)(GetScreenWidth() - 200)) , 480.0f);
    return _stat;
}

void Screen::Lobby::handleMouse(int *_currentScreen)
{
    (void) _currentScreen;
}

void Screen::Lobby::handleKeyboard(int *_currentScreen, bool *_sound_m)
{
    if (IsKeyPressed(KEY_K)) (*_sound_m) = !(*_sound_m);
    if (IsKeyPressed(KEY_P)) (*_currentScreen) = S_MENU;
    if (IsKeyPressed(KEY_N)) {
        prealoadGame();
        (*_currentScreen) = S_GAMEPLAY;
    }
}

void Screen::Lobby::drawScreen()
{
    // DrawTextureEx(_background, {0,0}, 0, 1.5, WHITE);
    DrawTextureEx(_logo, {(float)(GetScreenWidth() / 2 - _logo.width * 0.3 / 2), 140}, 0, 0.3, WHITE);

    Rectangle btnBounds1 = {(float)(GetScreenWidth() / 4) * 1 - 300, 650, 200, 40};
    Rectangle btnBounds2 = {(float)(GetScreenWidth() / 4) * 2 - 300, 650, 200, 40};
    Rectangle btnBounds3 = {(float)(GetScreenWidth() / 4) * 3 - 300, 650, 200, 40};
    Rectangle btnBounds4 = {(float)(GetScreenWidth() / 4) * 4 - 300, 650, 200, 40};

    Vector2 mousePoint = {0.0f, 0.0f};

    //----------------------------------------------------------------------------------
    mousePoint = GetMousePosition();
    int btnState1;
    int btnState2;
    int btnState3;
    int btnState4;

    static bool btnAction1 = false;
    static bool btnAction2 = false;
    static bool btnAction3 = false;
    static bool btnAction4 = false;

    // Check button state
    if (CheckCollisionPointRec(mousePoint, btnBounds1))
    {
        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT))
            btnState1 = 2;
        else
            btnState1 = 1;

        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            if (!btnAction1) {
                btnAction1 = true;
                _players[0] = 0;
                _nbPlayers++;
            } else {
                btnAction1 = false;
                _players[0] = -1;
                _nbPlayers--;
            }
        }
    }
    if (CheckCollisionPointRec(mousePoint, btnBounds2))
    {
        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT))
            btnState2 = 2;
        else
            btnState2 = 1;

        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            if (!btnAction2) {
                btnAction2 = true;
                _players[1] = 1;
                _nbPlayers++;
            } else {
                btnAction2 = false;
                _players[1] = -1;
                _nbPlayers--;
            }
        }
    }
    if (CheckCollisionPointRec(mousePoint, btnBounds3))
    {
        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT))
            btnState3 = 2;
        else
            btnState3 = 1;

        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            if (!btnAction3) {
                btnAction3 = true;
                _players[2] = 2;
                _nbPlayers++;
            } else {
                btnAction3 = false;
                _players[2] = -1;
                _nbPlayers--;
            }
        }
    }
    if (CheckCollisionPointRec(mousePoint, btnBounds4))
    {
        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT))
            btnState4 = 2;
        else
            btnState4 = 1;

        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT)) {
            if (!btnAction4) {
                btnAction4 = true;
                _players[3] = 3;
                _nbPlayers++;
            } else {
                btnAction4 = false;
                _players[3] = -1;
                _nbPlayers;
            }
        }
    }

    Texture2D text;
    DrawTextEx(_font, "Indie Studio", {(float)GetScreenWidth() / 2 - 80, 60}, 40, 1.0, RED);
    DrawTextureEx(_man, {(float)(GetScreenWidth() / 4) * 1 - 300, 330}, 0, 0.45, WHITE);
    DrawTextureEx(_girl, {(float)(GetScreenWidth() / 4) * 2 - 300, 330}, 0, 0.45, WHITE);
    DrawTextureEx(_man, {(float)(GetScreenWidth() / 4) * 3 - 300, 330}, 0, 0.45, WHITE);
    DrawTextureEx(_girl, {(float)(GetScreenWidth() / 4) * 4 - 300, 330}, 0, 0.45, WHITE);
    DrawRectangle((GetScreenWidth() / 4) * 1 - 300, 650, 200, 40, btnState1 == 1 ? GRAY : btnState1 == 2 ? GREEN : btnAction1  ? GREEN : RED);
    DrawRectangle((GetScreenWidth() / 4) * 2 - 300, 650, 200, 40, btnState2 == 1 ? GRAY : btnState2 == 2 ? GREEN : btnAction2 ? GREEN : RED);
    DrawRectangle((GetScreenWidth() / 4) * 3 - 300, 650, 200, 40, btnState3 == 1 ? GRAY : btnState3 == 2 ? GREEN : btnAction3 ? GREEN : RED);
    DrawRectangle((GetScreenWidth() / 4) * 4 - 300, 650, 200, 40, btnState4 == 1 ? GRAY : btnState4 == 2 ? GREEN : btnAction4 ? GREEN : RED);
    DrawText("<-- PRESS P to go to MENU", 50, GetScreenHeight() - 50, 30, BLACK);
    DrawText("PRESS N to go to GAMEPLAY -->",GetScreenWidth() - 550, GetScreenHeight() - 50, 30, BLACK);
    for (int i = 0; i != 4; i++)
    {
        bool isPlayer = false;
        for (size_t y = 0; y != _players.size(); y++) {
            if (i == _players[y])
                isPlayer = true;
        }
        if (isPlayer)
            DrawTextEx(_font, std::string("Player " + std::to_string(i + 1)).c_str(), {(float)((GetScreenWidth() / 4) * (i + 1) - 260), 655}, 30, 1, WHITE);
        else
            DrawTextEx(_font, std::string("IA " + std::to_string(i + 1)).c_str(), {(float)((GetScreenWidth() / 4) * (i + 1) - 230), 655}, 30, 1, WHITE);
    }
}
