/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Gameplay
*/

#include "Gameplay.hpp"
#include "BackupManager.hpp"

Screen::Gameplay::Gameplay()
    : _pause(false), _nb_player(0)
{
    _background = LoadTexture("../ressources/back.png");
    _mainBackgroundPlay = LoadTexture("../ressources/back.png");
    _recScore = LoadTexture("../ressources/textures/gameUI/rectscore.png");
    _rectScore = LoadTexture("../ressources/textures/gameUI/rectscore.png");
    BackupManager *b = new BackupManager();
    int load = 0;
    _map = new Game::Map(17, 13);
    std::vector<std::vector<Game::Block>> m = _map->getMap();

    for (int i = 0; i != m.size(); i++)
    {
        for (int a = 0; a != m[i].size(); a++)
        {
            if (m[i][a].getType() == Game::TYPE_BLOCK::PLAYER_BLOCK)
            {
                _p_chara.push_back(Vector3{(float)a, 0.0f, (float)i});
                _p_bomb.push_back(Vector3{(float)a, 1.4f, (float)i + 0.5f});
                _nb_player++;
            }
        }
    }
    

    initPLayerBomb(_nb_player);
    _rectScore.height *= 7;
    _rectScore.width *= 7;
    frames = 0;
    _isEnd = false;
    _isDraw = false;
    _r_lobby = (Rectangle) {(float) GetScreenWidth()/2 - 50, (float) GetScreenHeight()/2, 200, 50};
    _r_sound = (Rectangle) {(float) GetScreenWidth()/2 + 50, (float) GetScreenHeight()/2, 200, 50};
    b->saveGame(*this, "TEST");
}

Screen::Gameplay::~Gameplay()
{
    UnloadTexture(_background);
    UnloadTexture(_recScore);
}

void Screen::Gameplay::initGame(std::vector<Game::GameUI> loadstat)
{
    int load = 0;
    _stat = loadstat;
    for (int i = 0; i < 4; i++) {
        if (_stat[i].getIsPlayer())
            _characters.push_back(new Game::Character(i + 1, _p_chara[i], _p_bomb[i], &load, true));
        else
            _characters.push_back(new Game::Bot(i + 1, _p_chara[i], _p_chara, _bombs, &load));
    }
}

void Screen::Gameplay::handleMouse(int *_currentScreen)
{
    if (_isEnd == false && _isDraw == false) {
        if (_pause == true) {
            if (CheckCollisionPointRec(getPositionMouse(), _r_lobby)) {
                if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
                    (*_currentScreen) = S_LOBBY;
            }
        }
    }
}

void Screen::Gameplay::handleKeyboard(int *_currentScreen, bool *_sound_m)
{
    if (_isEnd == false && _isDraw == false) {
        if (_pause == false)
            update();
        else {
            if (IsKeyPressed(KEY_K)) (*_sound_m) = !(*_sound_m);
            if (CheckCollisionPointRec(getPositionMouse(), _r_sound)) {
                if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
                    (*_sound_m) = !(*_sound_m);
            }
        }
        if (IsKeyPressed(KEY_P)) _pause = !_pause;
    }
    else
        if (IsKeyPressed(KEY_B)) (*_currentScreen) = S_LOBBY;
}

void Screen::Gameplay::update(void)
{
    int a = -1;
    frames++;
    if (frames > 2000000)
    {
        frames = 0;
    }
    for (int i = 0; i < _nb_player; i++)
    {
        if (_characters[i]->getLife())
        {
            a = _map->isOnBonus(_p_chara[i].z, _p_chara[i].x);
            if (a == 0)
                addBombPlayer(i);
            if (a == 1)
                addRangePlayer(i);
            if (a == 2)
                _characters[i]->setVelocity(_characters[i]->getVelocity() + 0.05);
            if (i == 0 && _characters[i]->character == true)
                _characters[i]->move2(_map);
            else if (i == 1 && _characters[i]->character == true)
                _characters[i]->move(_map);
            else if (i == 2 && _characters[i]->character == true)
                _characters[i]->move3(_map);
            else if (i == 3 && _characters[i]->character == true)
                _characters[i]->move4(_map);
            else
                _characters[i]->move(_map);
            if (_characters[i]->checkIfDropBombe())
            {
                dropBomb(_p_chara[i], i);
            }
        }
    }
    checkBomb();
    moveExplosion(_map, _p_chara, _characters);
    for (int i = 0; i < 4; i++)
    {
        if (!_characters[i]->getLife())
            _p_chara[i].x = -1;
        _stat[i].setLife(_characters[i]->getLife());
        _stat[i].setBomb(getBombPlayer(i));
    }
    checkEnd();
}

void Screen::Gameplay::drawCharacters(void)
{
    for (int i = 0; i < _nb_player; i++)
    {
        if (i == 0 && _characters[i]->character == true)
            _characters[i]->draw1(getBombPlayer(i));
        else if (i == 1  && _characters[i]->character == true)
            _characters[i]->draw(getBombPlayer(i));
        else if (i == 2  && _characters[i]->character == true)
            _characters[i]->draw2(getBombPlayer(i));
        else if (i == 3  && _characters[i]->character == true)
            _characters[i]->draw(getBombPlayer(i));
        else 
            _characters[i]->draw(getBombPlayer(i));
    } 
    drawBomb();
}

void Screen::Gameplay::drawScreen(void)
{
    DrawTextureEx(_mainBackgroundPlay, {(float)0, 0}, 0, (float)GetScreenWidth() / 1300, WHITE);
    start3DMode();
    _map->Display();
    drawCharacters();
    end3DMode();
    for (int i = 0; i < 4; i++)
        _stat[i].drawUI();
    DrawTexture(_rectScore, 630, 20, WHITE);
    DrawText(TextFormat("TIME: %.02f", (float)frames/60), 670, 20, 20, BLACK);
    if (_pause == true) {
        _r_lobby = (Rectangle) {(float) GetScreenWidth()/2 - 250, (float) GetScreenHeight()/2, 200, 50};
        _r_sound = (Rectangle) {(float) GetScreenWidth()/2 + 150, (float) GetScreenHeight()/2, 200, 50};
        DrawText("PAUSE",GetScreenWidth()/2 - 10, GetScreenHeight()/2 - 80, 30, BLACK);
        DrawRectangle(_r_lobby.x, _r_lobby.y, _r_lobby.width, _r_lobby.height, GRAY);
        DrawRectangle(_r_sound.x, _r_sound.y, _r_sound.width, _r_sound.height, GRAY);
        DrawText("LOBBY", _r_lobby.x + 50, _r_lobby.y + 15, 30, WHITE);
        DrawText("SOUND", _r_sound.x + 50, _r_sound.y + 15, 30, WHITE);
    }
    if (_isEnd == true) {
        std::string winner;
        for (size_t i = 0; i < _characters.size(); i++) {
            if (_characters[i]->getLife() == 1)
                winner = _stat[i].getName();
        }
        DrawText(winner.c_str(), GetScreenWidth()/2 - 50, GetScreenHeight()/4 - 60, 50, BLACK);
        DrawText("WIN", GetScreenWidth()/2 - 50, GetScreenHeight()/4, 50, BLACK);
        if ((frames/30) % 2) DrawText("PRESS B to go back to LOBBY", GetScreenWidth() / 2 - 230, GetScreenHeight() / 10 * 9, 30, BLACK);
    }
    else if (_isDraw == true) {
        DrawText("DRAW", GetScreenWidth()/2 - 55, GetScreenHeight()/4, 50, BLACK);
        DrawText("NO WINNERS", GetScreenWidth()/2 - 55, GetScreenHeight()/4 + 100, 50, BLACK);
        if ((frames/30) % 2) DrawText("PRESS B to go back to LOBBY", GetScreenWidth() / 2 - 230, GetScreenHeight() / 10 * 9, 30, BLACK);
    }
}

void Screen::Gameplay::checkEnd()
{
    int alive = 4;

    for (size_t i = 0; i < _characters.size(); i++) {
        if (_characters[i]->getLife() == 0)
            alive--;
    }
    if (alive == 1) {
        _isEnd = true;
    }
    if (alive == 0) {
        _isDraw = true;
    }
}

std::vector<std::vector<Game::Block>> Screen::Gameplay::getVectorMap()
{
    return _map->getMap();
}

std::vector<Vector3> Screen::Gameplay::getCharacterPos(void)
{
    return _p_chara;
}
