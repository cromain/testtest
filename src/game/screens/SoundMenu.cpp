/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** SoundMenu
*/

#include "SoundMenu.hpp"

Screen::SoundMenu::SoundMenu()
{
    _moins = (Rectangle) {(float)GetScreenWidth() / 2 - 100, (float)GetScreenHeight() / 2, 50, 50};
    _plus = (Rectangle) {(float)GetScreenWidth() / 2 + 100, (float)GetScreenHeight() / 2, 50, 50};
    _music = LoadSound("../ressources/music/music1.mp3");
    PlaySound(_music);
    _volume = 1.00f;
}

Screen::SoundMenu::~SoundMenu()
{
}


void Screen::SoundMenu::drawScreen(void)
{
    DrawText("SOUND", (float)GetScreenWidth() / 2 - 80, (float)GetScreenHeight() / 5, 50, GRAY);
    DrawRectangle(_moins.x, _moins.y, _moins.width, _moins.height, GRAY);
    DrawRectangle(_plus.x, _plus.y, _plus.width, _plus.height, GRAY);
    DrawText(std::to_string(int(float(_volume * 100))).c_str(), (float) GetScreenWidth() / 2 - 30, (float)GetScreenHeight() / 2, 50, GRAY);
    DrawText("-", _moins.x + (_moins.width / 2) - 8, _moins.y + 10, 30, WHITE);
    DrawText("+", _plus.x + (_plus.width / 2) - 8, _plus.y + 10, 30, WHITE);
    DrawText("PRESS K to get out of the SOUND MENU", (float) GetScreenWidth() / 2 - 270, (float) GetScreenHeight() / 10 * 8, 30, BLACK);
}

void Screen::SoundMenu::handleKeyboard(int *_currentScreen, bool *_sound_m)
{
    (void)_currentScreen;
    if (IsKeyPressed(KEY_K)) (*_sound_m) = !(*_sound_m);
}

void Screen::SoundMenu::handleMouse(int *_currentScreen)
{
    (void)_currentScreen;
    _moins = (Rectangle) {(float)GetScreenWidth() / 2 - 100, (float)GetScreenHeight() / 2, 50, 50};
    _plus = (Rectangle) {(float)GetScreenWidth() / 2 + 50, (float)GetScreenHeight() / 2, 50, 50};

    if (CheckCollisionPointRec(GetMousePosition(), _moins)) {
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
            if (_volume > 0.00f) {
                _volume -= 0.01f;
                SetSoundVolume(_music, _volume);
            }
        }
    }
    if (CheckCollisionPointRec(GetMousePosition(), _plus)) {
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
            if (_volume < 1.00f) {
                _volume += 0.01f;
                SetSoundVolume(_music, _volume);
            }
        }
    }
}
