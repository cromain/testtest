/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Menu
*/

#include "Menu.hpp"

Screen::Menu::Menu()
{
    InitAudioDevice();
    _texture = LoadTexture("../ressources/textures/fondMenu.png");
    _font = LoadFont("../ressources/komika.png");
    _buttons.push_back(new Engine::Button(GetScreenWidth()/2 - 100, GetScreenHeight() / 2 - 100, "../ressources/textures/button_play/button_play.png", "../ressources/textures/button_play/button_play_1.png", "../ressources/textures/button_play/button_play_2.png", Engine::PLAY));
    _buttons.push_back(new Engine::Button(GetScreenWidth()/2 - 100, GetScreenHeight() / 2 + 20, "../ressources/textures/button_settings/button_settings.png", "../ressources/textures/button_settings/button_settings_1.png", "../ressources/textures/button_settings/button_settings_2.png", Engine::SETTINGS));
    _buttons.push_back(new Engine::Button(GetScreenWidth() - 60, 20, "../ressources/textures/button_quit/button_x.png", "../ressources/textures/button_quit/button_x_1.png", "../ressources/textures/button_quit/button_x_2.png", Engine::QUIT));
    _frames = 0;
}

Screen::Menu::~Menu()
{
    UnloadFont(_font);
    UnloadTexture(_texture);
}

void Screen::Menu::update(void)
{
    _buttons[0]->setPosition(GetScreenWidth()/2 - 100, GetScreenHeight() / 2 - 100);
    _buttons[1]->setPosition(GetScreenWidth()/2 - 100, GetScreenHeight() / 2 + 20);
    _buttons[2]->setPosition(GetScreenWidth() - 80, 30);
}

void Screen::Menu::drawScreen()
{
    _frames++;
    Color color;
    color.r = 197;
    color.g = 63;
    color.b =  35;
    color.a = 255;
    DrawTextureEx(_texture,{(float) -130, 0}, 0, (float) GetScreenHeight() / GetScreenWidth() * 1.4, WHITE);
    DrawTextEx(_font, TextSubtext("ManBomBer", 0, _frames/10), {(float) GetScreenWidth() / 2 - 80, 100}, 60, 1.0, YELLOW);
    for (int i = 0; i < 3; i++)
        _buttons[i]->draw();
}

void Screen::Menu::handleKeyboard(int *_currentScreen, bool *_sound_m)
{
    if (IsKeyPressed(KEY_K)) (*_sound_m) = !(*_sound_m);
    (void) _currentScreen;
}

void Screen::Menu::handleMouse(int *_currentScreen)
{
    update();
    for (int i = 0; i < 3; i++)
        _buttons[i]->setStatus(getPositionMouse(), _currentScreen);
}