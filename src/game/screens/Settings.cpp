/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Settings
*/

#include "Settings.hpp"

Screen::Settings::Settings()
{
    _font = LoadFont("../ressources/komika.png");
}

Screen::Settings::~Settings()
{
    UnloadFont(_font);
}

void Screen::Settings::handleMouse(int *_currentScreen)
{
    (void)_currentScreen;
}

void Screen::Settings::handleKeyboard(int *_currentScreen, bool *_sound_m)
{
    if (IsKeyPressed(KEY_K)) (*_sound_m) = !(*_sound_m);
    if (IsKeyPressed(KEY_P)) (*_currentScreen) = S_MENU;
}

void Screen::Settings::drawScreen(void)
{
    ClearBackground(ORANGE);
    DrawTextEx(_font, "MOVE FOREWARD",(Vector2) {(float) GetScreenWidth() / 4 - 200, 250}, 40, 1.0, BLACK);
    DrawTextEx(_font, "MOVE BACKWARDS",(Vector2) {(float) GetScreenWidth() / 4 - 200, 330}, 40, 1.0, BLACK);
    DrawTextEx(_font, "MOVE LEFT",(Vector2) {(float) GetScreenWidth() / 4 - 200, 410}, 40, 1.0, BLACK);
    DrawTextEx(_font, "MOVE RIGHT",(Vector2) {(float) GetScreenWidth() / 4 - 200, 490}, 40, 1.0, BLACK);
    DrawTextEx(_font, "DROP A BOMB",(Vector2) {(float) GetScreenWidth() / 4 - 200, 570}, 40, 1.0, BLACK);

    DrawTextEx(_font, "PLAYER 1                  PLAYER2", (Vector2) {(float) GetScreenWidth() / 3 * 2 - 350, 150}, 40, 1.0, BLACK);
    DrawTextEx(_font, "  Z                      UP ARROW",(Vector2) {(float) GetScreenWidth() / 3 * 2 - 300, 250}, 40, 1.0, BLACK);
    DrawTextEx(_font, "  S                     DOWN ARROW",(Vector2) {(float) GetScreenWidth() / 3 * 2 - 300, 330}, 40, 1.0, BLACK);
    DrawTextEx(_font, "  Q                     LEFT ARROW",(Vector2) {(float) GetScreenWidth() / 3 * 2 - 300, 410}, 40, 1.0, BLACK);
    DrawTextEx(_font, "  D                     RIGHT ARROW",(Vector2) {(float) GetScreenWidth() / 3 * 2 - 300, 490}, 40, 1.0, BLACK);
    DrawTextEx(_font, "SPACE                 RIGHT SHIFT",(Vector2) {(float) GetScreenWidth() / 3 * 2 - 320, 570}, 40, 1.0, BLACK);

    DrawTextEx(_font, "PRESS P to go back to MENU",(Vector2) {(float) GetScreenWidth() / 3 + 70, float(GetScreenHeight() - (GetScreenHeight()) / 7)}, 40, 1.0, RED);
}