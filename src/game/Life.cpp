/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Vie
*/

#include "Life.hpp"

Life::Life()
:_life(1)
{
}

Life::Life(int life)
:_life(life)
{
}

Life::~Life()
{
}

int Life::getLife() const
{
    return (_life);
}

void Life::setLife(int a)
{
    _life = a;
}

void Life::addLife(void)
{
    _life++;
}

void Life::decrementLife(void)
{
    if (_life != 0)
        _life--;
}