/*
** EPITECH PROJECT, 2021
** GameUI.cpp
** File description:
** GameUI
*/

#include "GameUI.hpp"
#include "general.hpp"

Game::GameUI::GameUI(std::string name)
{
    _name = name;
    _heart = LoadTexture("../ressources/textures/gameUI/heart.png");
    _heart.height /= 5;
    _heart.width /= 5;
    _bomb = LoadTexture("../ressources/textures/gameUI/bomb.png");
    _bomb.height /= 5;
    _bomb.width /= 5;
    _headskull = LoadTexture("../ressources/textures/gameUI/deathskull.png");
    _headskull.height /= 5;
    _headskull.width /= 5;
    _life = 1;
    _nbbomb = 1;
}

Game::GameUI::~GameUI()
{
}

bool Game::GameUI::getIsPlayer() {
    std::cout << _isplayer << std::endl;
    return _isplayer;
}

bool Game::GameUI::isPlayer(bool player) {
    std::cout << player << std::endl;
    _isplayer = player;
    return _isplayer;
}

void Game::GameUI::setLife(int life) {
    _life = life;
}

void Game::GameUI::setBomb(int bomb) {
    _nbbomb = bomb;
}

void Game::GameUI::posUI(float x, float y) {
    _posx = x;
    _posy = y;
}

void Game::GameUI::drawUI() {
    char liv[10];
    char bom[100];
    const char *name = _name.c_str();
    DrawText(name, _posx, _posy, 30, DARKGRAY);
    std::sprintf(liv, "x %d", _life);
    std::sprintf(bom, "x %d", _nbbomb);
    if (_life >= 1) {
        DrawTexture(_heart, _posx, _posy + 45, WHITE);
        DrawText(liv, _posx + 55, _posy + 55, 30, WHITE);
        DrawTexture(_bomb, _posx, _posy + 100, WHITE);
        DrawText(bom, _posx + 55, _posy + 110, 30, WHITE);
    }
    else
    {
        DrawTexture(_headskull, _posx, _posy + 45, WHITE);
    }
}