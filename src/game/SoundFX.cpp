/*
** EPITECH PROJECT, 2021
** Sound.hpp
** File description:
** Sound
*/

#include "SoundFX.hpp"

Game::SoundFX::SoundFX()
{
    _fxbomb = LoadSound("../ressources/sound/laser1.ogg");
}

Game::SoundFX::~SoundFX()
{
}

void Game::SoundFX::fx()
{
    PlaySound(_fxbomb);
}