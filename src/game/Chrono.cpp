/*
** EPITECH PROJECT, 2021
** B-YEP-400-LYN-4-1-indiestudio-romain.comtet
** File description:
** Chrono
*/

#include "Chrono.hpp"

Chrono::Chrono()
: _now(std::chrono::steady_clock::now())
{
}

std::chrono::steady_clock::time_point Chrono::now()
{
    return (std::chrono::steady_clock::now());
}

void Chrono::setChrono()
{
    _now = now();
}

std::chrono::steady_clock::time_point Chrono::getChrono()
{
    return (_now);
}

std::chrono::duration<double> Chrono::getTimerMillisecond(std::chrono::steady_clock::time_point t2) const
{
    return ((std::chrono::duration_cast<std::chrono::duration<double>>(t2 - _now)));
}