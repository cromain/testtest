/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"
#include "Game.hpp"
#include "Engine.hpp"
#include <stdlib.h>

int width = 17;
int height = 13;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    std::srand(std::time(nullptr));
    Game::Game game = Game::Game();

    while (!WindowShouldClose())
    {
        // if (IsKeyPressed(KEY_M))
        //     PlaySound(sound);
        game.handleEvent();
        game.displayScreen();
    }

    CloseWindow();

    return 0;
}

/* #################################################################################################################################################################################### */