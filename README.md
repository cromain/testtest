# Indie Studio

Indie Studio is a Epitech Project. The aim of the Indie Studio is to implement a cross-platform 3D video game with real-world tools.

We decide to implement our own game engine using the raylib library

## Installation

Prerequisites :

Use the software compilation tool [cmake](https://cmake.org/) to install Indie Studio.

If the directory "build" is not created, please create it :

```bash
mkdir "build"
```

Installation :

```bash
cd ./build
cmake ..
cmake --build .
```

## Usage

```bash'
WINDOWS : cd ./Debug
WINDOWS : ./indie_studio.exe
LINUX : ./indie_studio
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

loris.briere@epitech.eu | clement.guichard@epitech.eu | romain.comtet@epitech.eu | charles.gosseine@epitech.eu | thibault.barbe@epitech.eu | mahe.faure@epitech.eu